﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StartApp;
using System;

public class testads : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        /* StartAppWrapper.showAd()
      StartAppWrapper.addBanner( 
            StartAppWrapper.BannerType.AUTOMATIC,
            StartAppWrapper.BannerPosition.BOTTOM);*/
        //StartAppWrapper.AdEventListener
        EventListener listner = new EventListener();
        StartAppWrapper.init();
        StartAppWrapper.loadAd(StartAppWrapper.AdMode.OFFERWALL, listner);

        
#endif
    }
    public void ShowAd()
    {
#if UNITY_ANDROID
        StartAppWrapper.showAd();
        StartAppWrapper.loadAd(StartAppWrapper.AdMode.OFFERWALL);
#endif
    }

    // Update is called once per frame
    void Update()
    {

    }

    #if UNITY_ANDROID
    class EventListener : StartAppWrapper.AdEventListener
    {
        public void onFailedToReceiveAd()
        {

        }

        public void onReceiveAd()
        {

        }
    }
#endif
}
