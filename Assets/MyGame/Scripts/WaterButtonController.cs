﻿using CreativeSpore.SmartColliders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterButtonController : MonoBehaviour
{
    public WaterPoolController WaterPool;
    bool allowPushDown = true;
    void OnSmartCollisionStay2D(SmartCollision2D other)
    {
        // if (other.gameObject.CompareTag("Player") && allowPushDown && !WaterPool.Full)
        if (allowPushDown && !WaterPool.Full)
        {
            allowPushDown = false;
            GetComponent<Animator>().SetTrigger("Push");
            WaterPool.SendMessage("FillWater");
        }
    }
    public void AllowPushDown()
    {
        allowPushDown = true;
    }
}
