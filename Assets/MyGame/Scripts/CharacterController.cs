﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using CreativeSpore.SmartColliders;

public class CharacterController : MonoBehaviour
{

    public float MaxHP = 100;
    private float currentHP;
    public int Score;
    public int Gold;

    public bool Invulnerable;
    public bool InvulnerableEffect;
    public float InvulnerableTime;

    //public HealthBar healthBar;
    public Text goldText;
    public Collider2D radar;
    public bool IsActive = true;
    public bool Attacking;
    public bool CallingLance;
    public bool ReleaseLance;
    public bool Hurt;
    public float AttackRate = 1f;
    // public float CallLanceTime = 3f;
    // public float HoldlanceTime = 2f;
    public GameObject fire;
    public LanceController Lance;
    public Transform CheckPoint = null;

    private int gold;
    private SpriteRenderer spriteRenderer;
    private AudioSource hurtSound;
    private SmartRectCollider2D m_smartRectCollider;
    public PlatformCharacterController platformCharacterController;
    private Transform firePoint;
    private Transform LancePos;
    private Transform head;
    private float fireFraction;
    private float fireFractionSpeed = 0.2f;
    private float maxFireFraction = 0.24f;
    private float minFireFraction = -0.24f;
    private float lastTimeAttack;

    public float CurrentHP
    {
        get { return currentHP; }
        set
        {
            currentHP = value;
            //healthBar.SetPercent(currentHP / MaxHP);
        }
    }
    // Use this for initialization
    void Start()
    {
        //Application.targetFrameRate = 0;

        // CurrentHP = 100;
        spriteRenderer = GetComponent<SpriteRenderer>();
        hurtSound = GetComponent<AudioSource>();
        m_smartRectCollider = GetComponent<SmartRectCollider2D>();
        //SmartPlatformCollider c;
        // c.bot
        platformCharacterController = GetComponent<PlatformCharacterController>();
        firePoint = transform.Find("FirePoint");
        LancePos = transform.Find("Mit_Lance_Pos");
        head = transform.Find("Head");

    }

    public void PickupCoin(int gold)
    {
        this.gold += gold;
        goldText.text = "Gold: " + this.gold;
    }

    private IEnumerator InvulnerableEffectLoop(float delayedTime = 0.05f)
    {
        int i = 0;
        while (InvulnerableEffect)
        {
            yield return new WaitForSeconds(delayedTime);

            if (i++ % 2 == 0)
                spriteRenderer.color = new Color(1f, 1f, 1f, 0.3f);
            else
                spriteRenderer.color = new Color(1f, 1f, 1f, 1);

        }
        spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
    }
    private IEnumerator HitEffect(float delayedTime = 0.5f)
    {
        yield return new WaitForSeconds(delayedTime);
        //spriteRenderer.color = new Color(1f, 1f, 1f);
        Hurt = false;
        InvulnerableEffect = true;
        StartCoroutine(InvulnerableEffectLoop());
        yield return new WaitForSeconds(InvulnerableTime);
        Invulnerable = false;
        InvulnerableEffect = false;

    }

    public void Hit()
    {
        // SetNextState(eState.Dying);
        if (!Invulnerable)
        {
            Hurt = true;
            platformCharacterController.m_platformPhysics.VSpeed = 2;
            //hurtSound.Play();
            // SoundManager.Instance.mitHurt.Play();
            CurrentHP -= 5;
            Invulnerable = true;
            //spriteRenderer.color = new Color(1f, .5f, .5f);
            StartCoroutine(HitEffect(1));
        }
        //m_smartRectCollider.enabled = false;
        /* Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
         for (int i = 0; i < aColliders.Length; ++i)
         {
             aColliders[i].enabled = false;
         }
         transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
         m_rigidBody2D.velocity = Vector2.zero;
         //m_rigidBody2D.AddForce(new Vector2(0f, JumpSpeed), ForceMode2D.Impulse);

         StartCoroutine(RestartGame(1f));*/
    }

    // Update is called once per frame
    float attackCount;
    float callLanceCount;

    public void OnReleaseLanceEnd()
    {
        ReleaseLance = false;
    }
    void LateUpdate()
    {
        // Debug.Log(platformCharacterController.IsGrounded);
        /* if (!platformCharacterController.IsGrounded)
         {
             Debug.Log("!platformCharacterController.IsGrounded");
         }*/
        //Debug.Log(m_smartRectCollider.SkinRightRayContacts.Contains(true));
        /* if (platformCharacterController.GetActionState(eControllerActions.Jump) &&
             platformCharacterController.IsGrounded)
         {

         }*/
        if (Attacking && !Hurt)
        {
            attackCount += Time.deltaTime;
            callLanceCount += Time.deltaTime;
            ReleaseLance = false;//reset releaselance
            if (callLanceCount >= Lance.CallLanceTime)
            {
                //This time the Lance appear

                // callLanceCount -= CallLanceTime;
                // Lance.SetActive(true);
                //  Lance.transform.parent = LancePos.transform;
                //  Lance.transform.localPosition = Vector3.zero;
                //  Lance.transform.localRotation = Quaternion.identity;
                //  Lance.transform.localScale = new Vector3(1, 1, 1);
                Lance.Call(LancePos);
                //Lance.transform.parent = null;
                CallingLance = true;
                //Call lance now
            }
            else
            {
                if (attackCount >= AttackRate)
                {
                    attackCount -= AttackRate;
                    //GameObject fireObj = Instantiate(fire, firePoint.position, Quaternion.identity) as GameObject;
                    GameObject fireObj = TrashMan.spawn(fire, firePoint.position);
                    if (fireObj == null)
                        Debug.Log("Error here");
                    fireObj.GetComponent<MitFire1Controller>().SetDirection(fireFraction, transform.localScale.x);
                    //  SoundManager.Instance.shoot1.Play();
                    fireFraction += fireFractionSpeed;
                    if (fireFraction > maxFireFraction)
                    {
                        fireFractionSpeed *= -1;
                        fireFraction = maxFireFraction;
                    }
                    else if (fireFraction < minFireFraction)
                    {
                        fireFractionSpeed *= -1;
                        fireFraction = minFireFraction;
                    }
                    lastTimeAttack = Time.time;
                }
            }
        }
        else
        {
            if (Time.time - lastTimeAttack > 0.3f)
            {
                fireFraction = 0;
            }
            attackCount += Time.deltaTime;
            if (attackCount >= AttackRate)
                attackCount = AttackRate;

            /* if (ReleaseLance)
             {
                 ReleaseLance = false;
             }*/
            if (CallingLance)
            {
                if (Lance.Calling == LanceController.LanceState.CALLED)
                {
                    //now release the lance

                    Lance.Throw(transform.localScale.x);
                    ReleaseLance = true;
                }
                else
                {
                    Lance.Cancel();
                }
            }
            CallingLance = false;
            callLanceCount = 0;
        }



        if (IsActive)
        {
            if (!platformCharacterController.HasPlayerAbove)
                head.gameObject.SetActive(false);
            spriteRenderer.color = new Color(1, 1, 1, 1);
            //platformCharacterController.enabled = true;
          //  platformCharacterController.IsEnable = true;
        }
        else
        {
            head.gameObject.SetActive(true);
            spriteRenderer.color = new Color(1, 1, 1, 0.5f);
            //if (platformCharacterController.IsGrounded)
            //{
                // platformCharacterController.enabled = false;//Fix this, only disable when this player is below other
                //platformCharacterController.IsEnable = false;
            //}

        }
    }
    /* void FixedUpdate()
     {

     }*/


    public void Kill()
    {
        StartCoroutine(RestartGame(1f));
    }

    private IEnumerator RestartGame(float delayedTime = 1f)
    {
        yield return new WaitForSeconds(delayedTime);
        if (CheckPoint != null)
        {
            // SetNextState(eState.Idle);
            /* m_smartRectCollider.enabled = true;
             Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
             for (int i = 0; i < aColliders.Length; ++i)
             {
                 aColliders[i].enabled = true;
             }*/
            platformCharacterController.m_platformPhysics.VSpeed = 0;
            m_smartRectCollider.TeleportTo(new Vector3(CheckPoint.position.x, CheckPoint.position.y, transform.position.z));
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
        }

        yield return null;
    }
    /*  //In LayerCollision of smartContactPoint.otherCollider must have this gameobject.Layer
      void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
      {
          if (smartContactPoint.otherCollider.name.Equals("Mit_lance"))
          {
              float dot = Vector3.Dot(transform.up, smartContactPoint.normal);
              Debug.Log("dot : " + dot);
              smartContactPoint.otherCollider.gameObject.SendMessage("HitPlayer", SendMessageOptions.DontRequireReceiver);
          }
      }*/
}
