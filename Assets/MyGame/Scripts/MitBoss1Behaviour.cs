﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class MitBoss1Behaviour : MonoBehaviour
{
    public float HP = 1000;
    public float WalkSpeed = 0.1f;
    public float RunSpeed = 0.1f;
    public bool IsDying = false;
    public bool IsFriendly = false;
    public bool Invulnerable;
    public float WalkInterval = 1;
    public GameObject firePrefab;
    public Transform attackPos;
    private Rigidbody2D m_rigidBody2D;
    private Animator m_animator;
    private SmartRectCollider2D m_smartRectCollider;
    private SpriteRenderer spriteRenderer;
    private RingPath ringPath;
    EnemyHurtEffect hurt;
    // bool hurtingEffect;
    //int count;
    public enum State { Idle, Walk, Jump, Run };
    public State currentState = State.Idle;

    void Start()
    {
        m_rigidBody2D = GetComponent<Rigidbody2D>();
         m_animator = GetComponent<Animator>();
        m_smartRectCollider = GetComponent<SmartRectCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        hurt = GetComponent<EnemyHurtEffect>();
        ringPath = GetComponentInChildren<RingPath>();
        currentSpeed = WalkSpeed;
    }

    private float m_turnTimer = 0f;
    private float countTime = 0f;
    private bool radarInside;
    private bool canJump = true;

    [SerializeField]
    private float currentSpeed;
    void FixedUpdate()
    {
        if (!IsDying)
        {

            if (currentState == State.Idle)
            {

            }
            else
            if (currentState == State.Walk || currentState == State.Run)
            {




            }
            else if (currentState == State.Jump)
            {

            }



            // if (m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1 >= 0.6)
            {
                // m_rigidBody2D.AddForce(transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime, ForceMode2D.Impulse);
                // transform.position += transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime;
            }

        }
    }

    IEnumerator DelayToWalk(float delayedTime = 0.5f)
    {
        yield return new WaitForSeconds(delayedTime);
        currentState = State.Walk;
        currentSpeed = WalkSpeed;
    }

    //Called by animation vent
    public void StartFire()
    {
        GameObject fire = TrashMan.spawn(firePrefab, attackPos.position);
        Vector3 dir = GameManager.Instance.GirlPlayer.transform.position - attackPos.position;
        fire.GetComponent<EnemyFireController1>().SetDirection(dir.normalized);
       // SoundManager.Instance.hydraAttack.Play();
        ringPath.Continue();
    }
    public void Fire()
    {
        Debug.Log("Mit boss 1 Fire ");
        
        m_animator.SetTrigger("Attack");

    }
    public void OnRadarTriggerEnter()
    {

        radarInside = true;
    }
    public void OnRadarTriggerExit()
    {

        radarInside = false;
    }


    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        //return;
        //NOTE: dot product will be 1 if collision in perpendicular and opposite facing direction and 0 if horizontal and < 0 if perpendicular but in the same direction as facing direction
        float dot = Vector3.Dot(transform.up, smartContactPoint.normal);
        GameObject playerCtrl = smartContactPoint.otherCollider.gameObject;
        bool isPlayer = playerCtrl.tag == "Player";
        //Debug.Log("dot: " + dot);
        //Debug.DrawRay(smartContactPoint.point, smartContactPoint.normal, Color.white, 3f);
        if (isPlayer && !IsFriendly)
        {
            // if dot > 0, the collision is with top side
            if (dot > SmartRectCollider2D.k_OneSideSlopeNormThreshold)
            {
                // Kill the enemy, add player impulse                
                /* PlatformCharacterController platformCtrl = playerCtrl.GetComponent<PlatformCharacterController>();
                 if (platformCtrl)
                 {
                     platformCtrl.PlatformCharacterPhysics.Velocity = 1.5f * platformCtrl.JumpingSpeed * smartContactPoint.otherCollider.transform.up;
                 }
                 else
                 {
                     smartContactPoint.otherCollider.RigidBody2D.velocity = new Vector2(smartContactPoint.otherCollider.RigidBody2D.velocity.x, 0f);
                     smartContactPoint.otherCollider.RigidBody2D.AddForce(5f * smartContactPoint.otherCollider.RigidBody2D.transform.up, ForceMode2D.Impulse);
                 }*/
                // Kill();
                // m_smartRectCollider.enabled = false;
            }
            else
            {
                //Player was hit by this enemy
                playerCtrl.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    /*  void OnTriggerEnter2D(Collider2D other)
      {
          //Check this enemy was hit by Player's weapon
          if (other.CompareTag("PlayerWeapon") && !IsDying && !Invulnerable)
          {

              this.HP -= 10;
              SoundManager.Instance.hydraHurt.Play();
              if (this.HP <= 0)
              {
                  //GameObject.Instantiate(coin, transform.position, Quaternion.identity);
                  // hurtSound.Play();
                  IsDying = true;
                  Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
                  for (int i = 0; i < aColliders.Length; ++i)
                  {
                      aColliders[i].enabled = false;
                  }

                  GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
                  StartCoroutine(Kill());
              }

          }
      }*/

    public void Hit(float Damage)
    {
        if (!IsDying && !Invulnerable)
        {
            this.HP -= Damage;
            hurt.Hit(1);
           // SoundManager.Instance.hydraHurt.Play();
            if (this.HP <= 0)
            {
                //GameObject.Instantiate(coin, transform.position, Quaternion.identity);
                // hurtSound.Play();
                IsDying = true;
                Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
                for (int i = 0; i < aColliders.Length; ++i)
                {
                    aColliders[i].enabled = false;
                }

                GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
                StartCoroutine(Kill());
            }
        }

    }
    public IEnumerator Kill(float delayedTime = 0.5f)
    {
        GetComponent<EnemyMoneyDropper>().DropCoins();
        yield return new WaitForSeconds(delayedTime);
        Destroy(gameObject, 5f);
    }

}
