﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

    public int Level;
    public Sprite LockedSprite;
    public Sprite UnlockedSprite;
    bool unlocked;
    public void Click()
    {
        if (unlocked)
        {
            PlayerPrefs.SetInt(GameManager.KEY_MAP_INDEX, Level);
            SceneManager.LoadScene(1);
        }
    }
	// Use this for initialization
	void Start () {
       // PlayerPrefs.SetInt(GameManager.UNLOCKED_LEVEL_INDEX, 0);

        int unlockedLevel = PlayerPrefs.GetInt(GameManager.UNLOCKED_LEVEL_INDEX, 0);
		if (Level<= unlockedLevel)
        {
            GetComponent<Image>().sprite = UnlockedSprite;
            unlocked = true;
            GetComponentInChildren<TextMeshProUGUI>().SetText((Level+1)+"");
        }
        else
        {
            GetComponent<Image>().sprite = LockedSprite;
            unlocked = false;
            GetComponentInChildren<TextMeshProUGUI>().SetText("");
        }

        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
