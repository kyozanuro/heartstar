﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class WaterPoolController : MonoBehaviour
{

    float speed = 5;
    Transform Water;
    Transform EmptyPoint;
    Transform FullPoint;
    public bool Full;
    public float StepWaterHeight = 0.6f;
    public float StepTime = 2;
    // Use this for initialization
    void Start()
    { 
        Water = transform.Find("Water");
        EmptyPoint = transform.Find("EmptyPoint");
        FullPoint = transform.Find("FullPoint");
        Water.position = EmptyPoint.position;
        Full = false;
         
    }

    // Update is called once per frame
    /* void Update()
     {

     }*/

    public void FillWater()
    {
        if (Water.position.y < FullPoint.position.y)
        {
            Water.DOMoveY(StepWaterHeight, StepTime).SetRelative(true).OnUpdate(() =>
            {
                if (Water.position.y >= FullPoint.position.y)
                {
                    Full = true;
                    Water.position = new Vector3(Water.position.x, FullPoint.position.y, Water.position.z);
                }
            });
        }
    }
}
