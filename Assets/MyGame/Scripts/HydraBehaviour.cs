﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;
using System.Collections.Generic;

public class HydraBehaviour : MonoBehaviour
{
    public float HP = 200;
    public float WalkSpeed = 0.1f;
    public bool IsDying = false;
    public bool IsFriendly = false;
    public bool Invulnerable;
    public int NumberOfHeads = 4;
    public Rect BoundingBox;

    // public Sprite HeadSprite;
    private Rigidbody2D m_rigidBody2D;
    private Animator m_animator;
    private SmartRectCollider2D m_smartRectCollider;
    private HydraHeadBehaviour[] heads;
    Vector3 startPosition;
    // private EnemyMoneyDropper coinDropper;
    void Awake()
    {
        m_rigidBody2D = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_smartRectCollider = GetComponent<SmartRectCollider2D>();
        startPosition = transform.position;
        //coinDropper = GetComponent<EnemyMoneyDropper>();

        GameObject baseHead = transform.Find("BaseHead").gameObject;
        heads = new HydraHeadBehaviour[NumberOfHeads];
        heads[0] = baseHead.GetComponent<HydraHeadBehaviour>();
        float headHeight = baseHead.GetComponent<SpriteRenderer>().bounds.size.y;
        for (int i = 0; i < NumberOfHeads - 1; i++)
        {
            GameObject head = Instantiate(baseHead);
            heads[i + 1] = head.GetComponent<HydraHeadBehaviour>();
            head.transform.parent = transform;
            head.transform.localScale = new Vector3(1, 1, 1);
            head.transform.localPosition = new Vector3(baseHead.transform.localPosition.x, baseHead.transform.localPosition.y + headHeight * (i + 1), baseHead.transform.localPosition.z);


        }

    }

    void OnDisable()
    {
        transform.position = startPosition;
    }
    private float m_turnTimer = 0f;
    void FixedUpdate()
    {
        if (!IsDying)
        {
            if (m_turnTimer > 0f)
            {
                m_turnTimer -= Time.deltaTime;
            }

            if (m_turnTimer <= 0f &&
                !m_smartRectCollider.SkinBottomRayContacts[0] ||
                (m_smartRectCollider.SkinLeftRayContacts.Contains(true)) ||
               transform.position.x <= BoundingBox.min.x ||
               transform.position.x >= BoundingBox.max.x

                ) // or collision with front side NOTE: front is left side because Snail sprite is looking left
            {
                transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                m_turnTimer = 1f;
            }

            // if (m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1 >= 0.6)
            if (m_smartRectCollider.SkinBottomRayContacts[0])
            {
                // m_rigidBody2D.AddForce(transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime, ForceMode2D.Impulse);
                transform.position += transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime;
            }
        }

        // Debug.Log(m_smartRectCollider.SkinLeftRayContacts.Contains(true));
    }

    /*void Update()
    {
        float height = 2f * Camera.main.orthographicSize;
        float width = height * Camera.main.aspect;
        Debug.Log("width " + width +", height " + height);

        if (GetComponent<SpriteRenderer>().bounds.center.x < Camera.main.transform.position.x + width*0.5f)
        {
            this.gameObject.SetActive(true);
        }
        
    }
    */
    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        //return;
        //NOTE: dot product will be 1 if collision in perpendicular and opposite facing direction and 0 if horizontal and < 0 if perpendicular but in the same direction as facing direction
        float dot = Vector3.Dot(transform.up, smartContactPoint.normal);
        GameObject playerCtrl = smartContactPoint.otherCollider.gameObject;
        bool isPlayer = playerCtrl.tag == "Player";
        //Debug.Log("dot: " + dot);
        //Debug.DrawRay(smartContactPoint.point, smartContactPoint.normal, Color.white, 3f);
        if (isPlayer && !IsFriendly)
        {
            // if dot > 0, the collision is with top side
            if (dot > SmartRectCollider2D.k_OneSideSlopeNormThreshold)
            {
                // Kill the enemy, add player impulse                
                /* PlatformCharacterController platformCtrl = playerCtrl.GetComponent<PlatformCharacterController>();
                 if (platformCtrl)
                 {
                     platformCtrl.PlatformCharacterPhysics.Velocity = 1.5f * platformCtrl.JumpingSpeed * smartContactPoint.otherCollider.transform.up;
                 }
                 else
                 {
                     smartContactPoint.otherCollider.RigidBody2D.velocity = new Vector2(smartContactPoint.otherCollider.RigidBody2D.velocity.x, 0f);
                     smartContactPoint.otherCollider.RigidBody2D.AddForce(5f * smartContactPoint.otherCollider.RigidBody2D.transform.up, ForceMode2D.Impulse);
                 }*/
                // Kill();
                // m_smartRectCollider.enabled = false;
            }
            else
            {
                playerCtrl.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    

    public void OnTriggerEnterParent(HydraHeadBehaviour head, float damage)
    {
        // if (other.CompareTag("PlayerWeapon") && !IsDying && !Invulnerable)
        if (!IsDying && !Invulnerable)
        {
           // MitFire mitFire = other.gameObject.GetComponent<MitFire>();
           // if (mitFire != null && mitFire.CanDamage)
            {
                this.HP -= damage;
                heads[Random.Range(0, heads.Length)].Attack(-transform.localScale.x);
                head.PlayHurt();
                if (this.HP <= 0)
                {
                    //Enemy Dead
                    IsDying = true;
                    StartCoroutine(Kill());
                }
            }
            //GameObject.Instantiate(coin, transform.position, Quaternion.identity);
            // hurtSound.Play();
            // IsDying = true;
            /*  Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
              for (int i = 0; i < aColliders.Length; ++i)
              {
                  aColliders[i].enabled = false;
              }*/

            // GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
            // StartCoroutine(Kill());
        }
    }

    public IEnumerator Kill(float delayedTime = 0.5f)
    {
        GetComponent<EnemyMoneyDropper>().DropCoins();
        foreach (HydraHeadBehaviour head in heads)
        {
            head.DeadEffect();
        }
        yield return new WaitForSeconds(delayedTime);
        //  for (HydraHeadBehaviour head in heads)
        for (int i = 0; i < heads.Length; i++)
        //for (int i = heads.Length - 1; i >= 0; i--)
        {
            heads[i].Dead(i);
        }


        Destroy(gameObject, 5f);
    }

    /* void OnBecameVisible()
     {
         //Debug.Log(string.Format("{0} became visible", _objectName));
         //enabled = true;
         gameObject.SetActive(true);
     }
     void OnBecameInvisible()
     {
         gameObject.SetActive(false);
         // Debug.Log(string.Format("{0} became invisible", _objectName));
         // enabled = false;
     }*/
}
