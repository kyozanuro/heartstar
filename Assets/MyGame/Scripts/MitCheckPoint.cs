﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class MitCheckPoint : MonoBehaviour 
{

    public Sprite Activated;
    public Sprite Deactivated;
    public SpriteRenderer FlagSprRenderer;

    void Start()
    {
        FlagSprRenderer.sprite = Deactivated;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        CharacterController playerCtrl = other.gameObject.GetComponent<CharacterController>();
        if (playerCtrl != null)
        {
            if (playerCtrl.CheckPoint != null)
            {
                MitCheckPoint checkPoint = playerCtrl.CheckPoint.GetComponent<MitCheckPoint>();
                if( checkPoint != null )
                {
                    checkPoint.FlagSprRenderer.sprite = Deactivated;
                }
            }
            playerCtrl.CheckPoint = this.transform;
            FlagSprRenderer.sprite = Activated;
        }
    }
   /* void OnBecameVisible()
    {
        //Debug.Log(string.Format("{0} became visible", _objectName));
        enabled = true;
    }
    void OnBecameInvisible()
    {
        // Debug.Log(string.Format("{0} became invisible", _objectName));
        enabled = false;
    }*/
}
