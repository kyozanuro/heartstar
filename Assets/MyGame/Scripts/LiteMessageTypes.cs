﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiteMessageTypes
{
    public const int noParamMessage = 0;
    public const int gameObjectParamMessage = 1;
    public const int gameObjectIntParamMessage = 2;
    public const int gameObjectFloatParamMessage = 3;
}