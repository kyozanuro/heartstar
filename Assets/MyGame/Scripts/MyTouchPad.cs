using admob;

using ChartboostSDK;
using Facebook.Unity;
using Facebook.Unity.Example;
using StartApp;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput
{
    public class MyTouchPad : MonoBehaviour
    {
        public enum AxisOption
        {
            // Options for which axes to use
            Both, // Use both
            OnlyHorizontal, // Only horizontal
            OnlyVertical // Only vertical
        }
        public static bool LoadLevelSelect;
        public int MovementRange = 100;
        public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
        public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
        public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
        public string JumpName = "Jump";
        public string FireName = "Fire1";
        public Image leftButton;
        public Image rightButton;
        public Image jumpButton;
        public Image fireButton;

        public Sprite ArrowDown;
        public Sprite ArrowUp;
        public Sprite JumpUp;
        public Sprite JumpDown;

        public GameObject startMenuMap;
        public GameObject levelSelectMenuMap;
        public GameObject startItems;
        public GameObject backItems;
        public TextMeshProUGUI welcomeText;
        public Image profilePicture;
        public GameObject logoutPanel;
        public GameObject inviteButton;
        public GameObject mapSelectCanvas;
        public GameObject friendButtonPrefab;

        public GameObject updateScorePanel;
        Sprite defaultProfileSprite;
        Vector3 m_StartPos;
        public bool hasPublishPermission;
        bool m_UseX; // Toggle for using the x axis
        bool m_UseY; // Toggle for using the Y axis
        CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
        CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input


        bool touchLeft;
        bool touchRight;
        void OnEnable()
        {
            CreateVirtualAxes();
        }

        void Start()
        {


            if (defaultProfileSprite)
                defaultProfileSprite = profilePicture.sprite;
            if (welcomeText)
                welcomeText.SetText("");
            if (logoutPanel)
                logoutPanel.SetActive(false);
            if (inviteButton)
                inviteButton.SetActive(false);
            m_StartPos = transform.position;

            if (startMenuMap)
            {
                if (LoadLevelSelect)
                {

                    startMenuMap.SetActive(false);
                    startItems.SetActive(false);
                    backItems.SetActive(true);
                    levelSelectMenuMap.SetActive(true);
                }
                else
                {
                    startMenuMap.SetActive(true);
                    startItems.SetActive(true);
                    backItems.SetActive(false);
                    levelSelectMenuMap.SetActive(false);
                }
            }
            Facebook();
           // Ads();//Call this last

        }


        void Ads()
        {

#if UNITY_ANDROID
            if (!Chartboost.hasInterstitial(CBLocation.Default))
                Chartboost.cacheInterstitial(CBLocation.Default);
            
            StartAppWrapper.init();
            StartAppWrapper.loadAd(StartAppWrapper.AdMode.OFFERWALL);

#endif



#if UNITY_IOS
            Chartboost.cacheInterstitial(CBLocation.Default);
            Admob.Instance().initAdmob("ca-app-pub-2099434980306120/3094975098", "ca-app-pub-2099434980306120/4571708297");
            Admob.Instance().loadInterstitial();

#endif


        }

        void Facebook()
        {
            if (!FB.IsInitialized)
                FB.Init(this.OnInitComplete, this.OnHideUnity);

            if (FB.IsLoggedIn)
            {
                FB.API("/me", HttpMethod.GET, this.GetName);
                FB.API("/me/picture", HttpMethod.GET, this.ProfilePhotoCallback);
                inviteButton.SetActive(true);

                FB.API("/257639637996529/scores", HttpMethod.GET, this.GetFriendScores);
            }
        }

        public void FacebookButtonClick()
        {
            if (FB.IsLoggedIn)
            {
                this.CallFBLogout();
            }
            else
            {
                this.CallFBLogin();
            }
        }

        public void FacebookInviteButtonClick()
        {
            FB.Mobile.AppInvite(new Uri("https://fb.me/892708710750483"), callback: this.HandleResult);
        }
        private void CallFBLogin()
        {

            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, this.LoginHandleResult);
        }
        private void CallFBLoginForPublish()
        {
            // It is generally good behavior to split asking for read and publish
            // permissions rather than ask for them all at once.
            //
            // In your own game, consider postponing this call until the moment
            // you actually need it.
            FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, this.OnEnablePublishPermission);
        }

        private void CallFBLogout()
        {
            logoutPanel.SetActive(true);


        }

        public void FacebookConfirmLogoutYes()
        {
            FB.LogOut();
            profilePicture.sprite = defaultProfileSprite;
            welcomeText.SetText("");
            logoutPanel.SetActive(false);
            inviteButton.SetActive(false);
        }
        public void FacebookConfirmLogoutNo()
        {
            logoutPanel.SetActive(false);
        }

        public void CheckPostScorePermission()
        {
            //if (FB.IsLoggedIn)
            {
                //   hasPublishPermission = false;
                if (!hasPublishPermission)
                    FB.API("/me/permissions", HttpMethod.GET, this.OnCheckPermission);
                else
                {
                    int score = PlayerPrefs.GetInt(GameManager.UNLOCKED_LEVEL_INDEX, 0);
                    var scoreData = new Dictionary<string, string>() { { "score", score.ToString() } };
                    FB.API("/me/scores", HttpMethod.POST, this.ProfilePhotoCallback, scoreData);
                    // FB.API("/me/permissions", HttpMethod.GET, this.OnCheckPermission);
                }
            }
        }

        public void FacebookConfirmUpdateScoreProgressYes()
        {
            CallFBLoginForPublish();
            updateScorePanel.SetActive(false);
        }
        public void FacebookConfirmUpdateScoreProgressNo()
        {
            updateScorePanel.SetActive(false);
        }
        void OnEnablePublishPermission(IResult result)
        {
            int score = PlayerPrefs.GetInt(GameManager.UNLOCKED_LEVEL_INDEX, 0);
            var scoreData = new Dictionary<string, string>() { { "score", score.ToString() } };
            FB.API("/me/scores", HttpMethod.POST, this.ProfilePhotoCallback, scoreData);
        }
        void OnCheckPermission(IResult result)
        {
            List<object> list = result.ResultDictionary["data"] as List<object>;

            for (int i = 0; i < list.Count; i++)
            {
                Dictionary<string, object> dict = list[i] as Dictionary<string, object>;
                if (dict["permission"].ToString().Contains("publish_actions") &&
                    dict["status"].ToString().Contains("granted"))
                {
                    //already has publish permission
                    hasPublishPermission = true;
                }
            }

            if (!hasPublishPermission)
            {
                //show confirm dialog
                updateScorePanel.SetActive(true);
            }
        }
        //On Logged in 
        protected void LoginHandleResult(IResult result)
        {
            //get name & profile picture after logged in:
            FB.API("/me", HttpMethod.GET, this.GetName);
            FB.API("/me/picture", HttpMethod.GET, this.ProfilePhotoCallback);
            inviteButton.SetActive(true);

            FB.API("/257639637996529/scores", HttpMethod.GET, this.GetFriendScores);

        }

        private void GetFriendScores(IResult result)
        {
            Debug.Log(result.ResultDictionary);
            List<object> list = result.ResultDictionary["data"] as List<object>;
            Transform panel = mapSelectCanvas.transform.Find("Panel");
            for (int i = 0; i < list.Count; i++)
            {
                Dictionary<string, object> dict = ((list[i] as Dictionary<string, object>)["user"]) as Dictionary<string, object>;
                string name = dict["name"].ToString();
                string id = dict["id"].ToString();
                string score = (list[i] as Dictionary<string, object>)["score"].ToString();

                GameObject friendButton = Instantiate(friendButtonPrefab);
                //friendButton.transform.parent = mapSelectCanvas.transform;
                friendButton.GetComponentInChildren<FacebookFriendButton>().SetIdName(id, score, name, panel);
                /* RectTransform rectTransform = panel.GetChild(0).GetComponent<RectTransform>();
                 friendButton.GetComponent<RectTransform>().offsetMin = rectTransform.offsetMin;
                 friendButton.GetComponent<RectTransform>().offsetMax = rectTransform.offsetMax;
                 friendButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                 friendButton.GetComponent<RectTransform>().sizeDelta = friendButtonPrefab.GetComponent<RectTransform>().sizeDelta;*/
            }
        }
        protected void GetName(IResult result)
        {
            Debug.Log("name " + result.ResultDictionary["name"]);
            welcomeText.SetText(result.ResultDictionary["name"].ToString());
        }

        private void ProfilePhotoCallback(IGraphResult result)
        {
            Debug.Log(result.RawResult);
            if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
            {
                this.profilePicture.sprite = Sprite.Create(result.Texture, new Rect(0, 0, result.Texture.width, result.Texture.height), Vector2.zero);
            }

            this.HandleResult(result);
        }

        protected void HandleResult(IResult result)
        {

            if (result == null)
            {
                //this.LastResponse = "Null Response\n";
                //LogView.AddLog(this.LastResponse);
                return;
            }


            // welcomeText.SetText()
            LogView.AddLog(result.ToString());
            //   this.LastResponseTexture = null;

            // Some platforms return the empty string instead of null.
            /* if (!string.IsNullOrEmpty(result.Error))
             {
                 this.Status = "Error - Check log for details";
                 this.LastResponse = "Error Response:\n" + result.Error;
             }
             else if (result.Cancelled)
             {
                 this.Status = "Cancelled - Check log for details";
                 this.LastResponse = "Cancelled Response:\n" + result.RawResult;
             }
             else if (!string.IsNullOrEmpty(result.RawResult))
             {
                 this.Status = "Success - Check log for details";
                 this.LastResponse = "Success Response:\n" + result.RawResult;
             }
             else
             {
                 this.LastResponse = "Empty Response\n";
             }*/


        }

        private void OnInitComplete()
        {
            // this.Status = "Success - Check log for details";
            // this.LastResponse = "Success Response: OnInitComplete Called\n";
            string logMessage = string.Format(
                "OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
                FB.IsLoggedIn,
                FB.IsInitialized);
            LogView.AddLog(logMessage);
            if (AccessToken.CurrentAccessToken != null)
            {
                LogView.AddLog(AccessToken.CurrentAccessToken.ToString());
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            // this.Status = "Success - Check log for details";
            // this.LastResponse = string.Format("Success Response: OnHideUnity Called {0}\n", isGameShown);
            LogView.AddLog("Is game shown: " + isGameShown);
        }

        void Update()
        {
            if (touchLeft)
                m_HorizontalVirtualAxis.Update(-1);
            else if (touchRight)
                m_HorizontalVirtualAxis.Update(1);
            else
                m_HorizontalVirtualAxis.Update(0);

        }
        void UpdateVirtualAxes(Vector3 value)
        {
            var delta = m_StartPos - value;
            delta.y = -delta.y;
            delta /= MovementRange;
            if (m_UseX)
            {
                m_HorizontalVirtualAxis.Update(-delta.x);
            }

            if (m_UseY)
            {
                m_VerticalVirtualAxis.Update(delta.y);
            }
        }

        void CreateVirtualAxes()
        {
            // set axes to use
            m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
            m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

            // create new axes based on axes to use
            if (m_UseX)
            {
                m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
            }
            if (m_UseY)
            {
                m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
                CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
            }

        }

        public void Reset()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void BackToLevelSelect()
        {
            LoadLevelSelect = true;
            SceneManager.LoadScene(0);
        }
        public void Space()
        {

            GameManager.Instance.SwitchMap();
        }


        public void StartScene()
        {
            SceneManager.LoadScene(1);
        }

        public void StartLevelSelection()
        {
            startMenuMap.SetActive(false);
            startItems.SetActive(false);
            backItems.SetActive(true);
            levelSelectMenuMap.SetActive(true);
        }
        public void BackStartMenu()
        {
            startMenuMap.SetActive(true);
            startItems.SetActive(true);
            backItems.SetActive(false);
            levelSelectMenuMap.SetActive(false);
        }


        public void OnRightButtonDown()
        {
            //m_HorizontalVirtualAxis.Update(1);
            touchRight = true;
            touchLeft = false;

            // var pointer = new PointerEventData(EventSystem.current);
            // ExecuteEvents.Execute(leftButton.gameObject, pointer, ExecuteEvents.pointerUpHandler);

            rightButton.sprite = ArrowDown;
        }
        public void OnRightButtonUp()
        {
            //m_HorizontalVirtualAxis.Update(0);
            touchRight = false;
            rightButton.sprite = ArrowUp;
        }

        public void OnLeftButtonDown()
        {
            //m_HorizontalVirtualAxis.Update(-1);
            touchRight = false;
            touchLeft = true;
            leftButton.sprite = ArrowDown;
            //var pointer = new PointerEventData(EventSystem.current);
            // ExecuteEvents.Execute(rightButton.gameObject, pointer, ExecuteEvents.pointerUpHandler);

        }
        public void OnLeftButtonUp()
        {
            // m_HorizontalVirtualAxis.Update(0);
            touchLeft = false;
            leftButton.sprite = ArrowUp;
        }
        public void SetJumpDownState()
        {
            CrossPlatformInputManager.SetButtonDown(JumpName);
            jumpButton.sprite = JumpDown;
        }


        public void SetJumpUpState()
        {
            CrossPlatformInputManager.SetButtonUp(JumpName);
            jumpButton.sprite = JumpUp;
        }

        public void SetFireDownState()
        {
            CrossPlatformInputManager.SetButtonDown(FireName);
            fireButton.sprite = JumpDown;
        }


        public void SetFireUpState()
        {
            CrossPlatformInputManager.SetButtonUp(FireName);
            fireButton.sprite = JumpUp;
        }

        /*  public void OnDrag(PointerEventData data)
          {
              Vector3 newPos = Vector3.zero;

              if (m_UseX)
              {
                  int delta = (int)(data.position.x - m_StartPos.x);
                  delta = Mathf.Clamp(delta, - MovementRange, MovementRange);
                  newPos.x = delta;
              }

              if (m_UseY)
              {
                  int delta = (int)(data.position.y - m_StartPos.y);
                  delta = Mathf.Clamp(delta, -MovementRange, MovementRange);
                  newPos.y = delta;
              }
              transform.position = new Vector3(m_StartPos.x + newPos.x, m_StartPos.y + newPos.y, m_StartPos.z + newPos.z);
              UpdateVirtualAxes(transform.position);
          }
          */

        public void OnPointerUp(PointerEventData data)
        {
            transform.position = m_StartPos;
            UpdateVirtualAxes(m_StartPos);
        }


        public void OnPointerDown(PointerEventData data) { }

        void OnDisable()
        {
            // remove the joysticks from the cross platform input
            if (m_UseX)
            {
                m_HorizontalVirtualAxis.Remove();
            }
            if (m_UseY)
            {
                m_VerticalVirtualAxis.Remove();
            }
        }
    }
}