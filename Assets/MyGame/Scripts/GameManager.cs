﻿using admob;
using ChartboostSDK;
using CreativeSpore.SmartColliders;
using CreativeSpore.SuperTilemapEditor;
using StartApp;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class GameManager : Singleton<GameManager>
{
    protected GameManager()
    {

    }
    public const string KEY_MAP_INDEX = "MapIndex";
    public const string UNLOCKED_LEVEL_INDEX = "UnlockedLevelIndex";
    public MitCoinController MitCoin;
    public CharacterController GirlPlayer;
    public CharacterController BoyPlayer;
    public bool LoadFromEditor;
    public bool UnlockAllLevels;
    public bool EnableAds;
    public bool BlurryWorldObjects = true;//Object blurry when switched or completedly invisible?
    public int MapIndex = 0;
    public Map[] Maps;
    public Map currentMap;
    public GameObject girlMap;
    public GameObject boyMap;
    public GameObject sharedMap;
    public GameObject BlueBackground;
    public GameObject RedBackground;
    public Animator CanvasAnim;
    public Canvas CanvasBackground;
    public MyTouchPad MyTouchPad;

    bool currentMapIsGirl;
    int reachTargetCount;
    bool boyReachTarget;
    bool girlReachTarget;
    bool changingMap;
    Color blurColor = new Color(1, 1, 1, 0.3f);
    Color normalColor = new Color(1, 1, 1, 1f);
    float blurAlpha = 0.3f;
    float normalAlpha = 1;
    int blurSpriteRenderOrder = 0;
    int normalSpriteRenderOrder = 20;
    public bool CurrentMapIsGirl
    {
        set
        {
            currentMapIsGirl = value;
            // girlMap.GetComponent<Tilemap>().IsVisible = value;
            foreach (var tile in girlMap.GetComponentsInChildren<Tilemap>())
            {
                if (BlurryWorldObjects)
                {
                    if (value)
                    {
                        tile.TintColor = normalColor;
                    }
                    else
                    {
                        tile.TintColor = blurColor;
                    }
                }
                else
                    tile.IsVisible = value;

            }

            foreach (var spriteRenderer in girlMap.GetComponentsInChildren<SpriteRenderer>())
            {
                if (BlurryWorldObjects)
                {
                    if (value)
                    {
                        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, normalAlpha);
                        spriteRenderer.sortingOrder = normalSpriteRenderOrder;
                    }
                    else
                    {
                        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, blurAlpha);
                        spriteRenderer.sortingOrder = blurSpriteRenderOrder;
                    }
                }
                else
                    spriteRenderer.enabled = value;

            }
            //Disable children that not a map
            /* foreach (Transform child in girlMap.transform)
             {
                 if (child.gameObject.name.Contains("Disableable"))
                     child.gameObject.SetActive(value);
             }*/

            foreach (var tile in boyMap.GetComponentsInChildren<Tilemap>())
            {
                if (BlurryWorldObjects)
                {
                    if (value)
                        tile.TintColor = blurColor;
                    else
                        tile.TintColor = normalColor;
                }
                else
                    tile.IsVisible = !value;

            }
            foreach (var spriteRenderer in boyMap.GetComponentsInChildren<SpriteRenderer>())
            {
                if (BlurryWorldObjects)
                {
                    if (value)
                    {
                        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, blurAlpha);
                        spriteRenderer.sortingOrder = blurSpriteRenderOrder;
                    }
                    else
                    {
                        spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, normalAlpha);
                        spriteRenderer.sortingOrder = normalSpriteRenderOrder;
                    }

                }
                else
                    spriteRenderer.enabled = !value;

            }
            //Disable children that not a map
            /*  foreach (Transform child in boyMap.transform)
              {
                  if (child.gameObject.name.Contains("Disableable"))
                      child.gameObject.SetActive(!value);
              }*/

            // boyMap.GetComponent<Tilemap>().IsVisible = !value;
            GirlPlayer.IsActive = value;
            BoyPlayer.IsActive = !value;

        }
        get
        {
            return currentMapIsGirl;
        }
    }
    public override void Awake()
    {
        base.Awake();
        //ADS//

        /*
        #if UNITY_ANDROID && !UNITY_EDITOR

                StartAppWrapper.init();
                StartAppWrapper.loadAd(StartAppWrapper.AdMode.OFFERWALL);
                Chartboost.cacheInterstitial(CBLocation.Default);
        #endif

        #if UNITY_IOS
                Chartboost.cacheInterstitial(CBLocation.Default);
                // Admob.Instance().initAdmob("ca-app-pub-2099434980306120/3094975098", "ca-app-pub-2099434980306120/4571708297");
                // Admob.Instance().loadInterstitial();

        #endif*/


        //END - ADS//



        /* currentMap = Maps[0];
         girlMap = currentMap.GirlMap;
         boyMap = currentMap.BoyMap;
         sharedMap = currentMap.SharedMap;*/

        if (UnlockAllLevels)
            PlayerPrefs.SetInt(UNLOCKED_LEVEL_INDEX, Maps.Length - 1);
        foreach (Map map in Maps)
        {
            map.gameObject.SetActive(false);
        }
        /*#if !UNITY_EDITOR
                MapIndex = PlayerPrefs.GetInt(KEY_MAP_INDEX, 0);
        #endif
                ChangeMap(MapIndex);*/

        if (LoadFromEditor)
        {
            ChangeMap(MapIndex);
        }
        else
        {
            MapIndex = PlayerPrefs.GetInt(KEY_MAP_INDEX, 0);
            ChangeMap(MapIndex);
        }
    }

    public void SwitchMap()
    {
        if (!changingMap)
        {
            if (CurrentMapIsGirl && GirlPlayer.platformCharacterController.IsGrounded)
            {
                //now change to boy map (to blue)
                CanvasAnim.SetTrigger("ToBlue");
                BlueBackground.SetActive(true);
                RedBackground.SetActive(false);
                CurrentMapIsGirl = !CurrentMapIsGirl;
                SoundManager.Instance.switchMap.Play();
            }
            else if (BoyPlayer.platformCharacterController.IsGrounded)
            {
                //now change to girl map (to red)
                CanvasAnim.SetTrigger("ToRed");
                BlueBackground.SetActive(false);
                RedBackground.SetActive(true);
                CurrentMapIsGirl = !CurrentMapIsGirl;
                SoundManager.Instance.switchMap.Play();

            }



        }
    }
    public void ChangeMap(int index)
    {
        Maps[MapIndex].gameObject.SetActive(false);//disable previous index
        MapIndex = index;
        // PlayerPrefs.SetString("MapIndex",);
        PlayerPrefs.SetInt(KEY_MAP_INDEX, MapIndex);
        if (MapIndex > PlayerPrefs.GetInt(UNLOCKED_LEVEL_INDEX))
        {
            PlayerPrefs.SetInt(UNLOCKED_LEVEL_INDEX, MapIndex);
        }
        // Maps[index].gameObject.SetActive(true);
        currentMap = Maps[index];
        currentMap.ActionEnable += () =>
        {
            girlMap = currentMap.GirlMap;
            boyMap = currentMap.BoyMap;
            sharedMap = currentMap.SharedMap;
            CurrentMapIsGirl = true;
            BlueBackground.SetActive(false);
            RedBackground.SetActive(true);

        };
        currentMap.gameObject.SetActive(true);

        CanvasBackground.worldCamera = Camera.main;

        BoyPlayer.GetComponent<PlatformCharacterInput>().Enable();
        GirlPlayer.GetComponent<PlatformCharacterInput>().Enable();
        SoundManager.Instance.backgroundMusic.Play();
        // GirlPlayer.GetComponent<SmartPlatformCollider>().TeleportTo(currentMap.GirlStartPos.position);
        // BoyPlayer.GetComponent<SmartPlatformCollider>().TeleportTo(currentMap.BoyStartPos.position);
    }

    public void GirlReachTarget()
    {
        // reachTargetCount++;
        if (!changingMap)
            girlReachTarget = true;
    }
    public void GirlExitTarget()
    {
        if (!changingMap)
            girlReachTarget = false;
    }
    public void BoyReachTarget()
    {
        //reachTargetCount++;
        if (!changingMap)
            boyReachTarget = true;
    }
    public void BoyExitTarget()
    {
        if (!changingMap)
            boyReachTarget = false;
    }
    IEnumerator DelayChangeNextMap(float delayedTime = 1)
    {
        SoundManager.Instance.backgroundMusic.Pause();
        SoundManager.Instance.reachTarget.Play();


        MyTouchPad.CheckPostScorePermission();
        if (EnableAds)
        {
#if UNITY_ANDROID
            if (Chartboost.hasInterstitial(CBLocation.Default))
            {

                Chartboost.showInterstitial(CBLocation.Default);
                Chartboost.cacheInterstitial(CBLocation.Default);
            }
            else
            {

                //using StartApp as alternative on Android

                StartAppWrapper.showAd();
                StartAppWrapper.loadAd(StartAppWrapper.AdMode.OFFERWALL);

            }
#endif


#if UNITY_IOS
        //Admob is first priority in IOS
        if (Admob.Instance().isInterstitialReady())
        {
            Admob.Instance().showInterstitial();
            Admob.Instance().loadInterstitial();
        }
        else
        {
            //then Chartboost
            if (Chartboost.hasInterstitial(CBLocation.Default))
            {

                Chartboost.showInterstitial(CBLocation.Default);
                Chartboost.cacheInterstitial(CBLocation.Default);
            }
        }
#endif
        }
        yield return new WaitForSeconds(delayedTime);
        Debug.Log("Load next map now");
        changingMap = false;
        ChangeMap(MapIndex + 1);
    }

    void Update()
    {
        if (CrossPlatformInputManager.GetButtonDown("Switch"))
        {
            SwitchMap();
        }
        if (girlReachTarget && boyReachTarget && !changingMap)
        {
            girlReachTarget = false;
            boyReachTarget = false;
            changingMap = true;
            BoyPlayer.GetComponent<PlatformCharacterInput>().Disable();
            // BoyPlayer.GetComponent<PlatformCharacterController>().enabled = false;
            GirlPlayer.GetComponent<PlatformCharacterInput>().Disable();
            //GirlPlayer.GetComponent<PlatformCharacterController>().enabled = false;
            StartCoroutine(DelayChangeNextMap(SoundManager.Instance.reachTarget.clip.length));
            // reachTargetCount = 0;

        }
    }
}
