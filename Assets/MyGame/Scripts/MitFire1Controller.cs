﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MitFire))]
public class MitFire1Controller : MonoBehaviour
{
    //FIRE CONTROLLER ONLY CARE ABOUT HOW THE FIRE FLIES


    //public LayerMask hitDestroyLayers;
    protected float direction;
    protected MitFire mitFire;
    protected float fraction;
    protected Rigidbody2D rigid;
    // Use this for initialization
    public virtual void Awake()
    {
        mitFire = GetComponent<MitFire>();
        mitFire.Reset();
        rigid = GetComponent<Rigidbody2D>();
        // TrashMan.despawnAfterDelay(gameObject, 5);
        //Destroy(gameObject, 10);//auto destroy fire object after amount of time
        //Physics2D.IgnoreLayerCollision(gameObject.layer, 1<<gameObject.layer);
    }
    public virtual void SetDirection(float fraction, float direction)
    {
        this.fraction = fraction;
        this.direction = direction;
        mitFire.Reset();
    }
    // Update is called once per frame
    public virtual void FixedUpdate()
    {
       
        if (!mitFire.Hit)
        {
           /* if (Vector3.SqrMagnitude(this.transform.position - GameManager.Instance.MitPlayer.transform.position) > 10)
            {
                TrashMan.despawn(gameObject);
            }*/
            if (direction != 0)
            {
                rigid.velocity = Vector3.right * 2.5f * direction;
                //this.transform.position += Vector3.right * 1.5f * Time.deltaTime * direction;
            }

        }
    }
   
}
