﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class RatBehaviour : MonoBehaviour
{
    public float HP = 100;
    public float WalkSpeed = 0.1f;
    public float RunSpeed = 0.1f;
    public bool IsDying = false;
    public bool IsFriendly = false;
    public bool Invulnerable;
    public float WalkInterval = 1;
    private Rigidbody2D m_rigidBody2D;
    private Animator m_animator;
    private SmartRectCollider2D m_smartRectCollider;
    private SpriteRenderer spriteRenderer;
    // bool hurtingEffect;
    //int count;
    public enum State { Idle, Walk, Jump, Run };
    public State currentState = State.Idle;

    void Start()
    {
        m_rigidBody2D = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_smartRectCollider = GetComponent<SmartRectCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        currentSpeed = WalkSpeed;
    }

    private float m_turnTimer = 0f;
    private float countTime = 0f;
    private bool radarInside;
    private bool canJump = true;

    [SerializeField]
    private float currentSpeed;
    void FixedUpdate()
    {
        if (!IsDying)
        {
            /*if (m_turnTimer > 0f)
            {
                m_turnTimer -= Time.deltaTime;
            }*/

            if (currentState == State.Idle)
            {

            }
            else
            if (currentState == State.Walk || currentState == State.Run)
            {


                if ((!m_smartRectCollider.SkinBottomRayContacts[0] || // there is no a collision with floor
                       m_smartRectCollider.SkinLeftRayContacts.Contains(true))  // or collision with front side NOTE: front is left side because Snail sprite is looking left
                   )
                {
                    m_rigidBody2D.velocity = Vector2.zero;
                    transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
                    // m_turnTimer = WalkInterval;
                }
                m_rigidBody2D.AddForce(transform.localScale.x > 0 ? -transform.right * currentSpeed * Time.deltaTime : transform.right * currentSpeed * Time.deltaTime, ForceMode2D.Force);

            }
            else if (currentState == State.Jump)
            {
                m_rigidBody2D.AddForce(transform.localScale.x > 0 ? -transform.right * currentSpeed * Time.deltaTime : transform.right * currentSpeed * Time.deltaTime, ForceMode2D.Force);
                if (m_smartRectCollider.SkinBottomRayContacts[0] && m_rigidBody2D.velocity.y < 0)
                {
                    currentState = State.Walk;
                    currentSpeed = WalkSpeed;
                }
            }

            countTime += Time.deltaTime;
            if (countTime >= 3)
            {
                canJump = true;
                countTime -= 3;
                if (currentState == State.Idle)
                {
                    currentState = State.Walk;
                    Vector3 localScale = transform.localScale;
                    localScale.x = Random.Range(0, 2) == 0 ? -1 * localScale.x : 1 * localScale.x;
                    transform.localScale = localScale;
                }
                else if (currentState == State.Walk)
                    currentState = State.Idle;
            }

            if (currentState == State.Idle || currentState == State.Walk)
            {
                if (radarInside)
                {
                    /* if (Random.Range(0, 2) == 0)
                     {
                         currentState = State.Run;
                     }
                     else
                     {*/
                    if (canJump)
                    {
                        canJump = false;
                        currentState = State.Jump;
                        //Jump
                        m_rigidBody2D.AddForce(Vector3.up * 3, ForceMode2D.Impulse);
                    }
                    // }
                    currentSpeed = RunSpeed;
                }

            }
            if (currentState == State.Run)
            {
                if (!radarInside)
                {
                    StartCoroutine(DelayToWalk(1));
                }
            }

            if (m_animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1 >= 0.6)
            {
                // m_rigidBody2D.AddForce(transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime, ForceMode2D.Impulse);
                // transform.position += transform.localScale.x > 0 ? -transform.right * WalkSpeed * Time.deltaTime : transform.right * WalkSpeed * Time.deltaTime;
            }

        }
    }

    IEnumerator DelayToWalk(float delayedTime = 0.5f)
    {
        yield return new WaitForSeconds(delayedTime);
        currentState = State.Walk;
        currentSpeed = WalkSpeed;
    }
    public void OnRadarTriggerEnter()
    {
        //run 
        //Snail start to run when player in range
        // WalkSpeed = 2;
        //WalkInterval = 0.5f;
        // m_animator.speed = 10;
        radarInside = true;
    }
    public void OnRadarTriggerExit()
    {
        //walk 
        // WalkSpeed = 0.1f;
        // m_animator.speed = 1;
        radarInside = false;
    }

    /*void Update()
    {
        float height = 2f * Camera.main.orthographicSize;
        float width = height * Camera.main.aspect;
        Debug.Log("width " + width +", height " + height);

        if (GetComponent<SpriteRenderer>().bounds.center.x < Camera.main.transform.position.x + width*0.5f)
        {
            this.gameObject.SetActive(true);
        }
        
    }
    */
    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        //return;
        //NOTE: dot product will be 1 if collision in perpendicular and opposite facing direction and 0 if horizontal and < 0 if perpendicular but in the same direction as facing direction
        float dot = Vector3.Dot(transform.up, smartContactPoint.normal);
        GameObject playerCtrl = smartContactPoint.otherCollider.gameObject;
        bool isPlayer = playerCtrl.tag == "Player";
        //Debug.Log("dot: " + dot);
        //Debug.DrawRay(smartContactPoint.point, smartContactPoint.normal, Color.white, 3f);
        if (isPlayer && !IsFriendly)
        {
            // if dot > 0, the collision is with top side
            if (dot > SmartRectCollider2D.k_OneSideSlopeNormThreshold)
            {
                // Kill the enemy, add player impulse                
                /* PlatformCharacterController platformCtrl = playerCtrl.GetComponent<PlatformCharacterController>();
                 if (platformCtrl)
                 {
                     platformCtrl.PlatformCharacterPhysics.Velocity = 1.5f * platformCtrl.JumpingSpeed * smartContactPoint.otherCollider.transform.up;
                 }
                 else
                 {
                     smartContactPoint.otherCollider.RigidBody2D.velocity = new Vector2(smartContactPoint.otherCollider.RigidBody2D.velocity.x, 0f);
                     smartContactPoint.otherCollider.RigidBody2D.AddForce(5f * smartContactPoint.otherCollider.RigidBody2D.transform.up, ForceMode2D.Impulse);
                 }*/
                // Kill();
                // m_smartRectCollider.enabled = false;
            }
            else
            {
                //Player was hit by this enemy
                playerCtrl.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
            }
        }
    }

    /* void OnTriggerEnter2D(Collider2D other)
     {
         //Check this enemy was hit by Player's weapon
         if (other.CompareTag("PlayerWeapon") && !IsDying && !Invulnerable)
         {

             this.HP -= 10;
             SoundManager.Instance.hydraHurt.Play();
             if (this.HP <= 0)
             {
                 //GameObject.Instantiate(coin, transform.position, Quaternion.identity);
                 // hurtSound.Play();
                 IsDying = true;
                 Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
                 for (int i = 0; i < aColliders.Length; ++i)
                 {
                     aColliders[i].enabled = false;
                 }

                 GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
                 StartCoroutine(Kill());
             }

         }
     }*/
    public void Hit(float Damage)
    {
        if (!IsDying && !Invulnerable)
        {
            this.HP -= Damage;
           // SoundManager.Instance.hydraHurt.Play();
            if (this.HP <= 0)
            {
                //GameObject.Instantiate(coin, transform.position, Quaternion.identity);
                // hurtSound.Play();
                IsDying = true;
                Collider2D[] aColliders = GetComponentsInChildren<Collider2D>();
                for (int i = 0; i < aColliders.Length; ++i)
                {
                    aColliders[i].enabled = false;
                }

                GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
                StartCoroutine(Kill());
            }

        }    }

    public IEnumerator Kill(float delayedTime = 0.5f)
    {
        GetComponent< EnemyMoneyDropper>().DropCoins();
        yield return new WaitForSeconds(delayedTime);

        //transform.localScale = new Vector3(transform.localScale.x, -transform.localScale.y, transform.localScale.z);
       // transform.position = new Vector3(transform.position.x, transform.position.y, -1f);
       // m_rigidBody2D.velocity = Vector2.zero;
      //  m_rigidBody2D.AddForce(transform.up, ForceMode2D.Impulse);
        Destroy(gameObject, 5f);
    }

    /* void OnBecameVisible()
     {
         //Debug.Log(string.Format("{0} became visible", _objectName));
         //enabled = true;
         gameObject.SetActive(true);
     }
     void OnBecameInvisible()
     {
         gameObject.SetActive(false);
         // Debug.Log(string.Format("{0} became invisible", _objectName));
         // enabled = false;
     }*/
}
