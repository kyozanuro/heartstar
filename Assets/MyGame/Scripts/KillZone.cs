﻿using CreativeSpore.SmartColliders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour {

    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        GameObject playerCtrl = smartContactPoint.otherCollider.gameObject;
        bool isPlayer = playerCtrl.CompareTag("Player");
        if (isPlayer)
        {
            playerCtrl.SendMessage("Kill", SendMessageOptions.DontRequireReceiver);
        }
    }
  /*  void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("KillZone OnTriggerEnter2D");
    }*/
}
