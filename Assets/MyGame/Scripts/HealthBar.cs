﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {

    float maxWidth;
    public void SetPercent(float percent)
    {
      //  Rect rect = GetComponent<RectTransform>().rect;
      //  rect.width = percent * maxWidth;
        GetComponent<RectTransform>().sizeDelta = new Vector2( percent * maxWidth, GetComponent<RectTransform>().sizeDelta.y);
    }
	// Use this for initialization
	void Awake () {
        maxWidth = GetComponent<RectTransform>().rect.width;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
