﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadFollow : MonoBehaviour {

    public Transform follower;

    Vector3 distance;
    void Awake()
    {
        distance =  transform.position -follower.position;
    }
    void FixedUpdate()
    {
        transform.position = follower.position + distance;
    }
}
