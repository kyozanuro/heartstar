﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHurtEffect : MonoBehaviour
{
    public Color effect1;
    public Color effect2;
    public GameObject traceSpriteRenderer;

    List<SpriteRenderer> listRenderers;
    SpriteRenderer spriteRenderer;
    Animator animator;
    bool hurtingEffect;
    int count;
    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        listRenderers = new List<SpriteRenderer>();
        animator = GetComponent<Animator>();
        listRenderers.Add(spriteRenderer);
        if (traceSpriteRenderer)
            listRenderers.AddRange(traceSpriteRenderer.GetComponentsInChildren<SpriteRenderer>());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        count++;

        if (hurtingEffect)
        {
            if (animator)
                animator.enabled = false;
            foreach (SpriteRenderer spriteRenderer in listRenderers)
            {
                if (count % 4 == 0)
                    spriteRenderer.color = effect2;
                else
                    spriteRenderer.color = effect1;
            }
        }
        else
        {
            if (animator)
                animator.enabled = true;
            foreach (SpriteRenderer spriteRenderer in listRenderers)
            {
                spriteRenderer.color = Color.white;
            }
        }

    }

    IEnumerator HurtEnd(float delayedTime = 0.4f)
    {
        yield return new WaitForSeconds(delayedTime);
        hurtingEffect = false;
    }

    public void Hit(float damage)
    {
        if (!hurtingEffect)
        {
            hurtingEffect = true;
            StartCoroutine(HurtEnd());
        }
    }
    /* void OnTriggerEnter2D(Collider2D other)
     {
         //Check this enemy was hit by Player's weapon
         if (other.CompareTag("PlayerWeapon"))
         {
             if (!hurtingEffect)
             {
                 hurtingEffect = true;
                 StartCoroutine(HurtEnd());
             }
         }
     }*/
}
