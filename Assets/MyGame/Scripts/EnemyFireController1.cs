﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class EnemyFireController1 : MonoBehaviour
{

    //public LayerMask hitDestroyLayers;
    public float speed = 1;
    Vector3 direction;
    LayerMask wallMask;
    bool despawn;
    // Use this for initialization
    void Start()
    {
        wallMask = LayerMask.NameToLayer("Wall");
        //Physics2D.IgnoreLayerCollision(gameObject.layer, 1<<gameObject.layer);
    }
    public void SetDirection(Vector3 direction)
    {
        this.direction = direction;
        despawn = false;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.SqrMagnitude(this.transform.position - GameManager.Instance.GirlPlayer.transform.position) > 20)
        {
            if (!despawn)
            {
                TrashMan.despawn(gameObject);
                despawn = true;
            }
        }

        //if (direction != 0)
        this.transform.position += (speed * Time.deltaTime * direction);
    }

    /*  void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
      {
          Debug.Log("hit OnSmartTriggerStay2D" + smartContactPoint.otherCollider.gameObject.name);
      }*/
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == wallMask || other.CompareTag("Player"))
        {
            // Debug.Log("hit OnTriggerEnter2D" + other.name);
            other.gameObject.SendMessage("Hit", SendMessageOptions.DontRequireReceiver);
            if (!despawn)
            {
                TrashMan.despawn(gameObject);
                despawn = true;
            }
            // Destroy(gameObject);
        }
    }

    /* void OnCollisionEnter2D(Collision2D coll)
     {
         Debug.Log("hit OnCollisionEnter2D" + coll.gameObject.name);

     }*/
}
