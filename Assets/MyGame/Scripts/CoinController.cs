﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class CoinController : MonoBehaviour
{
    bool destroyed;
    
    // Use this for initialization
    void Start()
    {
        PlanetBehaviour planet = FindObjectOfType<PlanetBehaviour>();
        if (planet)
        {
            Vector2 vDiff = FindObjectOfType<PlanetBehaviour>().transform.position - transform.position;
            float rot_z = Mathf.Atan2(vDiff.y, vDiff.x) * Mathf.Rad2Deg;
            Quaternion dstRot = Quaternion.Euler(0f, 0f, rot_z + 90);
            transform.rotation = dstRot;
        }
        GetComponent<Rigidbody2D>().AddForce(transform.up * 7, ForceMode2D.Impulse);
    }

    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        if (!destroyed)
        {
            destroyed = true;
            GameObject playerCtrl = smartContactPoint.otherCollider.gameObject;
            bool isPlayer = playerCtrl.tag == "Player";
            if (isPlayer)
            {
                playerCtrl.SendMessage("PickupCoin", SendMessageOptions.DontRequireReceiver);
                GetComponent<AudioSource>().Play();
                GetComponent<SpriteRenderer>().enabled = false;
            }
            Debug.Log("pickup coin");
            Destroy(this.gameObject, 0.5f);
        }
    }
}
