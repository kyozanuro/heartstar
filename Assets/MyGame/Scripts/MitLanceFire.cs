﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MitLanceFire : MitFire
{
    LanceController lance;
    protected override void Awake()
    {
        base.Awake();
        lance = GetComponentInParent<LanceController>();
    }
    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyInvurnerable") && lance.Calling == LanceController.LanceState.THROWING)
        {
            lance.HitInvurnableEnemy();
            //SoundManager.Instance.fireBack.Play();
        }
    }
}
