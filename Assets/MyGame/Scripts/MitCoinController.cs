﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MitCoinController : MonoBehaviour
{

    public int InitLevel = 0;
    public float speed = 2;
    public float TimeToDisappear = 8;
    Rigidbody2D rigidbody2d;
    Animator animator;
    SpriteRenderer spriteRenderer;
    Vector2 direction;
    LayerMask wallLayer;
    float count;
    float timeCount;
    float currentSpeed;
    bool stopMoveTowardPlayer = true;
    bool pickedUp;
    int hit = 0;
    int level = 0;
    int[] coinByLevel = new int[] { 20, 50, 100, 200, 500, 1000 };
    // Use this for initialization
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
       // SoundManager.Instance.coinStart.Play();
        wallLayer = LayerMask.NameToLayer("Wall");
        level = Mathf.Clamp(InitLevel, 0, 5);
        animator.SetInteger("Level", level);
        hit = 0;

        rigidbody2d.AddForce(transform.up * 1.4f, ForceMode2D.Impulse);
    }

    public void SetLevel(int level)
    {
        level = Mathf.Clamp(level, 0, 5);
        animator.SetInteger("Level", level);
        hit = 0;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (pickedUp)
        {

            rigidbody2d.AddForce(200 * Vector2.up * Time.deltaTime, ForceMode2D.Force);
        }
        else
        {
            timeCount += Time.deltaTime;
            if (timeCount >= TimeToDisappear - 5)
            {
                //2 secs before disappear;
                spriteRenderer.color = new Color(1, 1, 1, Random.Range(0.2f, 1f));
            }
            if (timeCount >= TimeToDisappear)
            {
                // Destroy(gameObject);
                TrashMan.despawn(gameObject);
            }
            if (!stopMoveTowardPlayer)
            {
                direction = GameManager.Instance.GirlPlayer.transform.position - this.transform.position;
                //  Debug.Log("direction.sqrMagnitude " + direction.sqrMagnitude + ", " + currentSpeed);
                currentSpeed = Mathf.Clamp(speed / direction.sqrMagnitude, 250f, 700f);


                // if (direction.sqrMagnitude < 2)
                //    speed *= 2;
                Vector2 dir = new Vector2(0, 0);
                if (direction.x < 0)
                    dir.x = -1;
                else
                    dir.x = 1;
                rigidbody2d.velocity = new Vector2(0, rigidbody2d.velocity.y);
                rigidbody2d.AddForce(currentSpeed * dir * Time.deltaTime, ForceMode2D.Force);
            }
            else
            {
                count += Time.deltaTime;
                if (count >= 2)
                {
                    stopMoveTowardPlayer = false;
                }
            }
        }
    }
    /* int getGoldByLevel()
     {
         if (this.level == 0)
             return 20;
         if (this.level == 1)
             return 50;
         if (this.level == 2)
             return 100;
         if (this.level == 3)
             return 200;
         if (this.level == 4)
             return 500;
         if (this.level == 5)
             return 1000;
         return 0;
     }*/

    void CheckLevel()
    {
        if (hit == 3)
        {
            level++;
            if (level > coinByLevel.Length - 1)
                level = coinByLevel.Length - 1;


            animator.SetInteger("Level", level);
            hit = 0;
        }
    }

    public void CoinIncrementEnter()
    {

        rigidbody2d.velocity = Vector2.zero;
        rigidbody2d.AddForce(transform.up * 1.6f, ForceMode2D.Impulse);
        stopMoveTowardPlayer = true;
        count = 0;
        hit++;
        timeCount += Mathf.Clamp(0.3f * (level + 1), 0.3f, 1.2f);
        // Debug.Log("CoinIncrementEnter" + hit);
        CheckLevel();
       // SoundManager.Instance.coinHit.Play();
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        // Debug.Log("other OnTriggerEnter2D" + other.gameObject.name);
        //Player pickup this coin
        if (other.CompareTag("Player") && !pickedUp)
        {
            other.gameObject.SendMessage("PickupCoin", coinByLevel[level], SendMessageOptions.DontRequireReceiver);
           // SoundManager.Instance.coinPick.Play();
            animator.SetBool("Disappear", true);
            pickedUp = true;
            rigidbody2d.velocity = Vector2.zero;
            Collider2D[] cols = GetComponentsInChildren<Collider2D>();
            foreach(Collider2D col in cols)
            {
                col.enabled = false;
            }
           // test.enabled = false;
            // Destroy(gameObject, 2);
            TrashMan.despawnAfterDelay(gameObject, 2);
        }
    }

    void OnCollisionStay2D(Collision2D coll)
    {

        if (rigidbody2d.velocity.y == 0)
        {
            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 1.4f);
        }
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
       // Debug.Log("coll " + coll.gameObject.name + ", v " + rigidbody2d.velocity);
        if (rigidbody2d.velocity.y > 2.2)
        {
            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 2.2f);
        }
        if (rigidbody2d.velocity.y < -2.2f)
        {
            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, -2.2f);
        }
        float absy = Mathf.Abs(rigidbody2d.velocity.y);
        if (absy < 1.4f)
        {
            if (rigidbody2d.velocity.y != 0)
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 1.4f * (rigidbody2d.velocity.y) / absy);
            else
                rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, 1.4f);


            // float forceY = (rigidbody2d.velocity.y) / (Mathf.Abs(rigidbody2d.velocity.y));
            // rigidbody2d.AddForce(new Vector2(0, -forceY * 1.5f), ForceMode2D.Impulse);


        }
        /* if (rigidbody2d.velocity.y > -1f)
         {
             rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, -1f);
         }*/
        if (coll.gameObject.layer == wallLayer)
        {
            // float forceY = (rigidbody2d.velocity.y) / (Mathf.Abs(rigidbody2d.velocity.y));
            // rigidbody2d.AddForce(new Vector2(0, -forceY), ForceMode2D.Impulse);
            stopMoveTowardPlayer = false;
        }
    }
}
