﻿using UnityEngine;
using System.Collections;

public class SoundManager : Singleton<SoundManager>
{
    protected SoundManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public string myGlobalVar = "whatever";
    public AudioSource backgroundMusic;
    public AudioSource jump;
    public AudioSource switchMap;
    public AudioSource reachTarget;

}