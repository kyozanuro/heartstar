﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FacebookFriendButton : MonoBehaviour
{

    string id;
    string namee;
    TextMeshProUGUI nameText;
    Image avatarImage;
    Transform panel;



    public void SetIdName(string id, string score, string name, Transform panel)
    {
        this.id = id;
        Debug.Log("score " + score);
        nameText = GetComponentInChildren<TextMeshProUGUI>();
        avatarImage = GetComponent<Image>();

        this.namee = name;
        this.panel = panel;
        nameText.SetText(name);
        FB.API("/" + id + "/picture", HttpMethod.GET, this.GetFriendPicture);
        // FB.API("/" + id + "/scores", HttpMethod.GET, this.GetFriendScoreLevel);
        int iscore;
        bool success = int.TryParse(score, out iscore);
        if (!success || iscore < 0) iscore = 0;
        if (iscore >= panel.childCount) iscore = panel.childCount - 1;
        //if (iscore < panel.childCount)
        {

            RectTransform rectTransform = panel.GetChild(iscore).GetComponent<RectTransform>();
            RectTransform thisRectTransform = GetComponent<RectTransform>();
            this.transform.SetParent(panel);
            thisRectTransform.offsetMin = rectTransform.offsetMin;
            thisRectTransform.offsetMax = rectTransform.offsetMax;
            Debug.Log("offsetMin" + rectTransform.offsetMin);
            Debug.Log("offsetMax" + rectTransform.offsetMax);
            thisRectTransform.localScale = new Vector3(1, 1, 1);
            thisRectTransform.sizeDelta = new Vector2(60, 60);
            this.transform.SetParent(panel.parent);

        }
       

    }
    void GetFriendPicture(IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
        {
            this.avatarImage.sprite = Sprite.Create(result.Texture, new Rect(0, 0, result.Texture.width, result.Texture.height), Vector2.zero);
        }

    }
    /*  void GetFriendScoreLevel(IResult result)
      {
          string score = ((result.ResultDictionary["data"] as List<object>)[0] as Dictionary<string, object>)["score"].ToString();
          Debug.Log("score " + score);
          int iscore = int.Parse(score);
          if (iscore < panel.childCount)
          {
              RectTransform rectTransform = panel.GetChild(iscore).GetComponent<RectTransform>();
              RectTransform thisRectTransform = GetComponent<RectTransform>();
              thisRectTransform.offsetMin = rectTransform.offsetMin;
              thisRectTransform.offsetMax = rectTransform.offsetMax;
              thisRectTransform.localScale = new Vector3(1, 1, 1);
              thisRectTransform.sizeDelta = new Vector2(60, 60);
          }
      }*/
    // Update is called once per frame
    void Update()
    {

    }
}
