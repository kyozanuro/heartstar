﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoBehaviour
{
    CharacterController mitPlayer;
    //List<SpriteRenderer> listSprites;
    void Awake()
    {
        mitPlayer = GameManager.Instance.GirlPlayer.GetComponent<CharacterController>();
        /* listSprites = new List<SpriteRenderer>();
         foreach (Transform child in this.transform)
         {
             listSprites.Add(child.GetComponent<SpriteRenderer>());
         }*/
        foreach (Transform spriteChild in transform)
        {
            spriteChild.gameObject.SetActive(false);
        }
        StartCoroutine(CheckObjectInCameraSize());
    }


    IEnumerator CheckObjectInCameraSize()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.3f);
            foreach (Transform spriteChild in transform)
            {
                float height = 2f * Camera.main.orthographicSize;
                float width = height * Camera.main.aspect;
                // Debug.Log("width " + width + ", height " + height);
                //SpriteRenderer spriteChild = child.GetComponent<SpriteRenderer>();
                if (spriteChild != null)
                {
                    if (spriteChild.position.x <= mitPlayer.radar.bounds.max.x //0.6 for a bit far than the camera view
                        && spriteChild.position.x >= mitPlayer.radar.bounds.min.x)
                    {
                        spriteChild.gameObject.SetActive(true);
                    }


                    if (spriteChild.position.x > mitPlayer.radar.bounds.max.x + mitPlayer.radar.bounds.size.x //0.6 for a bit far than the camera view
                       || spriteChild.position.x < mitPlayer.radar.bounds.min.x - mitPlayer.radar.bounds.size.x)
                    {
                        spriteChild.gameObject.SetActive(false);
                    }
                }
            }
            // Debug.Log("Run CHeck EnemeyManager");

        }
    }
}
