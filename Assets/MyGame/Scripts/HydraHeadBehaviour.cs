﻿using UnityEngine;
using System.Collections;
using CreativeSpore.SmartColliders;

public class HydraHeadBehaviour : MonoBehaviour
{
    public HydraBehaviour parent;
    public Transform firePoint;
    public EnemyFireController1 firePrefab;
    bool isAttacking;
    void Awake()
    {


    }

    bool randomBollean()
    {
        return (Random.value > 0.5f);
    }
    public void Attack(float direction)
    {
        if (!isAttacking)
        {
            isAttacking = true;
            StartCoroutine(attack(direction));
        }

    }
    public IEnumerator attack(float direction, float delayedTime = 0.8f)
    {
        yield return new WaitForSeconds(delayedTime);
        GetComponent<Animator>().SetTrigger("Attack");
        //SoundManager.Instance.hydraAttack.Play();
        // EnemyFireController1 fire1 = Instantiate(firePrefab, firePoint.position, Quaternion.identity) as EnemyFireController1;
        GameObject fire1 = TrashMan.spawn(firePrefab.gameObject, firePoint.position);
        Vector3 v3dir = new Vector3(1, 0, 0);
        if (direction > 0) v3dir.x = 1;
        else if (direction < 0) v3dir.x = -1;
        fire1.GetComponent<EnemyFireController1>().SetDirection(v3dir);
        isAttacking = false;
    }
    //Call before Dead()
    public void DeadEffect()
    {
        GetComponent<Animator>().SetBool("Dead", true);
        this.gameObject.layer = LayerMask.NameToLayer("Radar");//change to this layer to avoid collision with fires
    }
    IEnumerator delayDead(float delayedTime = 0.1f)
    {
        yield return new WaitForSeconds(delayedTime);
        Rigidbody2D rigidbody = gameObject.AddComponent<Rigidbody2D>();
        rigidbody.angularDrag = 0;
        rigidbody.drag = 0;
        //rigidbody.AddForce(Random.insideUnitCircle *1.5f, ForceMode2D.Impulse);
        float rand = Random.Range(0.1f, 0.3f);
        if (randomBollean())
            rand *= -1;
        // Debug.Log("rand " + rand);
        rigidbody.AddTorque(rand, ForceMode2D.Impulse);
    }

    //Call after DeadEffect
    public void Dead(int i)
    {
        transform.parent = null;
       
        GetComponent<BoxCollider2D>().isTrigger = false;
        StartCoroutine(delayDead(i * 0.1f));


    }
    public void PlayHurt()
    {
        GetComponent<Animator>().SetTrigger("IsHurt");
       // SoundManager.Instance.hydraHurt.Play();
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        //Destroy the head when it collide with Wall (to create dead effect)
        if (coll.collider.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            StartCoroutine(delayDestroy());
        }
    }
    IEnumerator delayDestroy(float delayedTime = 0.1f)
    {
        yield return new WaitForSeconds(delayedTime);
        GetComponent<BoxCollider2D>().isTrigger = true;//Make the head fall off the walll
        Destroy(gameObject, 5f);
    }
    //Being hit by player's weapon
    /*void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("other OnTriggerEnter2D" + other.gameObject.name);
        if (other.CompareTag("PlayerWeapon"))
        {
            parent.OnTriggerEnterParent(this, other);//call parent method to reduce HP
           // GetComponent<Animator>().SetTrigger("IsHurt");
           // SoundManager.Instance.hydraHurt.Play();
            //GetComponent<SpriteRenderer>().color = new Color(1, 0.5f, 0.5f);
        }
    }*/

    //Called By Mit
    public void Hit(float Damage)
    {
        parent.OnTriggerEnterParent(this, Damage);//call parent method to reduce HP
    }

}
