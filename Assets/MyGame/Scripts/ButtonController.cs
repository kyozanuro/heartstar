﻿using CreativeSpore.SmartColliders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    public Animator Button;
    public GameObject[] Lasers;
    bool pushed;
    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        //if (other.CompareTag("Player"))
        if (!pushed)
        {
            pushed = true;
            GetComponent<Animator>().SetTrigger("Push");
            if (Button)
                Button.SetTrigger("Close");
            foreach (GameObject laser in Lasers)
            {
                laser.SetActive(false);
            }
        }
    }
}
