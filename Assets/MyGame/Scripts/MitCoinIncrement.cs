﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MitCoinIncrement : MonoBehaviour
{
    MitCoinController coin;
    MitFire fire;
    void Awake()
    {
        coin = GetComponentInParent<MitCoinController>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("OnTriggerEnter2D Mit Increment " + other.gameObject.name);
        // if (other.CompareTag("PlayerWeapon") && other.gameObject != fire)
        if (other.CompareTag("PlayerWeapon"))
        {
            fire = other.GetComponent<MitFire>();
            if (fire != null)
            {
                if (fire.CanDamage)
                {
                    coin.CoinIncrementEnter();
                    other.gameObject.SendMessage("HitCoin", SendMessageOptions.DontRequireReceiver);
                }
            }
        }
        /* Debug.Log("other OnTriggerEnter2D" + other.gameObject.name);
         if (other.CompareTag("PlayerWeapon"))
         {

             rigidbody2d.velocity = Vector2.zero;
             rigidbody2d.AddForce(transform.up * 2, ForceMode2D.Impulse);
             stopMoveTowardPlayer = true; count = 0;
             SoundManager.Instance.coinHit.Play();
         }*/
    }


}
