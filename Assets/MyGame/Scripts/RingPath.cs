﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingPath : MonoBehaviour
{

    public DOTweenPath[] paths;
    public Transform IK;
    DOTweenPath current;
    MitBoss1Behaviour boss;
    // Use this for initialization
    void Start()
    {
        boss = GetComponentInParent<MitBoss1Behaviour>();
       // DOTweenPath path;
       // path.relative = true;
       /* for (int i = 0; i < paths.Length; i++)
        {
           paths[i].relative = true;
           //paths[i].duration = 1;
        }*/

        /* DOTweenPath path = paths[Random.Range(0, paths.Length)];
         IK.parent = path.gameObject.transform;
         IK.localPosition = Vector3.zero;
         path.DOPlay();*/
        StartCoroutine(CompleteDelay());
    }
    //When the path is complete, will call this
    public void OnCompletePath()
    {
        boss.Fire();
       

    }
    public void Continue()
    {
        StartCoroutine(CompleteDelay(0));
    }

    IEnumerator CompleteDelay(float delayedTime = 1.3f)
    {
        yield return new WaitForSeconds(delayedTime);
        current = paths[Random.Range(0, paths.Length)];
        //path.gameObject.transform.position = IK.position;
        IK.parent = current.gameObject.transform;
       // yield return new WaitForSeconds(1);
        IK.localPosition = Vector3.zero;
        current.DORestart();
        // path.DOPlay();
    }

    // Update is called once per frame
    void Update()
    {
       /* if (current)
        {
            Vector3 velocity = Vector3.zero;
            IK.transform.position = Vector3.SmoothDamp(IK.transform.position, current.transform.position,ref velocity, 0.1f);
        }*/
    }
}
