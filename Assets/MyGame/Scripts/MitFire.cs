﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MitFire : MonoBehaviour
{

    public float Damage;
    public bool CanDamage = true;
    public bool AutoDestroy = true;//Auto destroy/despawn on hit coin, wall, enemy
    protected LayerMask wallMask;
    //public bool Stop;
    public bool Hit;
    bool destroyed;
    Rigidbody2D rigid;
    Collider2D collider2d;
    // Use this for initialization
    protected virtual void Awake()
    {
        wallMask = LayerMask.NameToLayer("Wall");
        rigid = GetComponent<Rigidbody2D>();
        collider2d = GetComponent<Collider2D>();
    }

    void FixedUpdate()
    {
        if (AutoDestroy &&
            Vector3.SqrMagnitude(this.transform.position - GameManager.Instance.GirlPlayer.transform.position) > 10)
        {
            if (!destroyed)
            {
                TrashMan.despawn(gameObject);
                destroyed = true;
            }
        }
    }
    //Reset after spawn object (because of using pooling)
    public void Reset()
    {
        // Stop = false;
        destroyed = false;
        Hit = false;
        collider2d.enabled = true;
        collider2d.isTrigger = true;
        GetComponent<Animator>().ResetTrigger("Hit");
        //  Rigidbody2D rigid = GetComponent<Rigidbody2D>();
        rigid.gravityScale = 0;
        rigid.velocity = Vector2.zero;
        rigid.angularVelocity = 0;
        this.gameObject.layer = LayerMask.NameToLayer("Fire");
    }


    public void HitCoin()
    {
        //Destroy(gameObject);
        if (AutoDestroy && !destroyed)
        {
            TrashMan.despawnAfterDelay(gameObject, 0.3f);
            GetComponent<Animator>().SetTrigger("Hit");
            rigid.velocity = Vector2.zero;
            collider2d.enabled = false;
            Hit = true;
            destroyed = true;
        }
    }
    protected virtual void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.layer == wallMask || other.CompareTag("Enemy"))
        {
            //Hit Wall or Enemy
            if (AutoDestroy && !destroyed)
            {

                TrashMan.despawnAfterDelay(gameObject, 0.3f);
                rigid.velocity = Vector2.zero;
                GetComponent<Animator>().SetTrigger("Hit");
                collider2d.enabled = false;
                Hit = true;
                destroyed = true;
            }

            if (other.CompareTag("Enemy"))
            {
                if (CanDamage)
                    other.gameObject.SendMessage("Hit", Damage, SendMessageOptions.DontRequireReceiver);
            }
        }
        else if (other.CompareTag("EnemyInvurnerable"))
        {
            if (!Hit)
            {
                //Rigidbody2D rigid = GetComponent<Rigidbody2D>();
                Vector2 force = rigid.velocity * -0.2f;
                collider2d.isTrigger = false;
                rigid.velocity = Vector2.zero;
                rigid.angularVelocity = 0;
                rigid.gravityScale = 1;
                rigid.AddForce(force, ForceMode2D.Impulse);
                Hit = true;
                this.gameObject.layer = LayerMask.NameToLayer("Radar");
               // SoundManager.Instance.fireBack.Play();
                // Stop = true;
                if (AutoDestroy && !destroyed)
                {
                    destroyed = true;
                    TrashMan.despawnAfterDelay(gameObject, 1f);
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("hit " + coll.gameObject.name);

    }



}
