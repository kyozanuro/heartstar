﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MitBoss1Heart : MonoBehaviour {

    MitBoss1Behaviour boss;
    void Awake()
    {
        boss = GetComponentInParent<MitBoss1Behaviour>();
    }
	public void Hit(float Damage)
    {
        boss.Hit(Damage);
    }
}
