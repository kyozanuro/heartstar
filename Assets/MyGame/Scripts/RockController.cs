﻿using CreativeSpore.SmartColliders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockController : MonoBehaviour
{

    public Transform Rock;
    public float RotateSpeed;
    public float WaterDrag = 10;
    public float NormalDrag = 1;
    Rigidbody2D rigidbody2d;
    public LayerMask WaterLayers;

    bool isInsideWaterMaskTrigger;
    bool isInsideWaterTrigger;
    bool persit;
    float persitTime;
    Vector3 previousPos;
    // Use this for initialization
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Rock.Rotate(Vector3.forward, -RotateSpeed * rigidbody2d.velocity.x);
        if (isInsideWaterMaskTrigger && isInsideWaterTrigger)
        {
            rigidbody2d.drag = WaterDrag;
        }
        else
        {
            rigidbody2d.drag = NormalDrag;
        }

        if (rigidbody2d.velocity.y < -1)
        {
            rigidbody2d.mass = 0.1f;
        }
        else
        {
            rigidbody2d.mass = 1;
        }
        if (rigidbody2d.velocity.y <= -2f)
        {
            rigidbody2d.velocity = new Vector2(rigidbody2d.velocity.x, -2);
        }
        // persit = false;
        /*  if (persit)
              rigidbody2d.isKinematic = true;
          else
              rigidbody2d.isKinematic = false;
          Debug.Log("Test " + (Time.fixedTime - persitTime));
          if (Time.fixedTime - persitTime >= 5)
          {
              persit = false;
          }*/
    }

    //  void FixedUpdate()
    // {
    //if (persit)
    //  transform.position = previousPos;

    // previousPos = transform.position;


    //  }

    void OnCollisionStay2D(Collision2D other)
    // void OnSmartCollisionStay2D(SmartCollision2D other)

    {
        if (LayerMask.LayerToName(other.gameObject.layer).Contains("Rock"))
        {
          //  rigidbody2d.velocity = Vector2.zero;
          //  rigidbody2d.angularVelocity = 0;
          ////  rigidbody2d.drag = 100000000000;
           // rigidbody2d.angularDrag = 100000000000;
          //  rigidbody2d.gravityScale = 0;
           // persit = true;
            //persitTime = Time.fixedTime;

        }
    }
    void OnCollisionExit2D(Collision2D other)
    {
        if (LayerMask.LayerToName(other.gameObject.layer).Contains("Rock"))
        {
           // rigidbody2d.drag = NormalDrag;

        }
    }

    IEnumerator delayCollisionExit()
    {
        yield return new WaitForSeconds(1f);
        persit = false;

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if ((WaterLayers & (1 << other.gameObject.layer)) != 0)
        {
            if (other.gameObject.name.Equals("Water"))
            {
                isInsideWaterTrigger = true;
            }
            else if (other.gameObject.name.Equals("WaterMask"))
            {
                isInsideWaterMaskTrigger = true;
            }

            //  rigidbody2d.velocity = Vector3.zero;
            //  rigidbody2d.drag = WaterDrag;

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if ((WaterLayers & (1 << other.gameObject.layer)) != 0)
        {
            if (other.gameObject.name.Equals("Water"))
            {
                isInsideWaterTrigger = false;
            }
            else if (other.gameObject.name.Equals("WaterMask"))
            {
                isInsideWaterMaskTrigger = false;
            }

            // rigidbody2d.drag = NormalDrag;
        }
    }
}
