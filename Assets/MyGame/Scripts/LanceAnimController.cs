﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanceAnimController : MonoBehaviour {

	public void Hide()
    {
        GetComponentInParent<LanceController>().Hide();
    }
}
