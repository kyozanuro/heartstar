﻿using CreativeSpore.SmartColliders;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour {

    public GameObject GirlMap;
    public GameObject BoyMap;
    public GameObject SharedMap;
    public Transform GirlStartPos;
    public Transform BoyStartPos;
    public Action ActionEnable;
    //GameObject ccamera;
    // Use this for initialization
    void Start () {
       

    }

    void OnEnable()
    {
        if (!GirlMap)
        {
            GirlMap = transform.Find("GirlMap").gameObject;
            BoyMap = transform.Find("BoyMap").gameObject;
            SharedMap = transform.Find("SharedMap").gameObject;
            GirlStartPos = transform.Find("GirlStartPos");
            BoyStartPos = transform.Find("BoyStartPos");
            //ccamera = transform.Find("PixelPerfectCamera").gameObject;
        }
        //Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, Camera.main.transform.position.z);
        GameManager.Instance.BoyPlayer.GetComponent<SmartPlatformCollider>().TeleportTo(BoyStartPos.position);
        GameManager.Instance.GirlPlayer.GetComponent<SmartPlatformCollider>().TeleportTo(GirlStartPos.position);
        ActionEnable.Invoke();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
