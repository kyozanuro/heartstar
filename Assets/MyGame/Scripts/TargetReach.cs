﻿using CreativeSpore.SmartColliders;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetReach : MonoBehaviour
{
    /* void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
     {
         if (smartContactPoint.otherCollider.name.Equals("GirlPlayer"))
         {
             GameManager.Instance.GirlReachTarget();
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
            // collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
         }
         else if (smartContactPoint.otherCollider.name.Equals("BoyPlayer"))
         {
             GameManager.Instance.BoyReachTarget();
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
             //collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
         }
     }*/
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.name.Equals("GirlPlayer"))
        {
            if (GameManager.Instance.GirlPlayer.platformCharacterController.IsGrounded)
            {
                GameManager.Instance.GirlReachTarget();
            }
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
            // collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
        }
        else if (other.name.Equals("BoyPlayer"))
        {
            if (GameManager.Instance.BoyPlayer.platformCharacterController.IsGrounded)
            {
                GameManager.Instance.BoyReachTarget();
            }
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
            //collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.name.Equals("GirlPlayer"))
        {
            GameManager.Instance.GirlExitTarget();
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
            // collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
        }
        else if (other.name.Equals("BoyPlayer"))
        {
            GameManager.Instance.BoyExitTarget();
            // collision.gameObject.GetComponent<SmartCollider2D>().enabled = false;
            //collision.gameObject.GetComponent<PlatformCharacterController>().enabled = false;
        }
    }
}