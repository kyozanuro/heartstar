﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class EnemyMoneyDropper : MonoBehaviour
{
    public Vector3 dropOffset;
    public int[] coinLevels;
    public int[] coinQuantities;
    private int[] realCoinQuantities;
    void Awake()
    {

        realCoinQuantities = new int[coinLevels.Length];
        coinQuantities.CopyTo(realCoinQuantities, 0);
        for (int i = 0; i < realCoinQuantities.Length; i++)
        {
            if (realCoinQuantities[i] <= 0)
                realCoinQuantities[i] = 1;
        }
    }

    public void DropCoins()
    {
        for (int i = 0; i < realCoinQuantities.Length; i++)
        {
            for (int j = 0; j < realCoinQuantities[i]; j++)
            {
                StartCoroutine(DelayedCoinGen(coinLevels[i],i*j, Random.Range(0, 0.5f)));
            }

        }
    }
    IEnumerator DelayedCoinGen(int level,int index, float delayedTime = 0.5f)
    {
        if (index == 0)
            delayedTime = 1;
        yield return new WaitForSeconds(delayedTime);

        // MitCoinController coin = Instantiate(GameManager.Instance.MitCoin, transform.position, Quaternion.identity) as MitCoinController;
        MitCoinController coin =TrashMan.spawn(GameManager.Instance.MitCoin.gameObject, transform.position).GetComponent<MitCoinController>();
        coin.SetLevel(level);
        coin.transform.position += new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), 0) + dropOffset;
    }

   /* public void OnSceneGUI()
    {
        Handles.color = Color.green;
        Vector3 vBodyHandler = Handles.FreeMoveHandle(transform.position, Quaternion.identity, 0.15f * HandleUtility.GetHandleSize(transform.position), Vector3.zero, Handles.SphereCap);
    }*/
  /*  void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.05f);
    }*/
}
