﻿using UnityEngine;
using System.Collections;


public class MitFire2Controller :MitFire1Controller
{
    //FIRE CONTROLLER ONLY CARE ABOUT HOW THE FIRE FLIES


    //public LayerMask hitDestroyLayers;
    //-1 -> 1
    Vector3 v_direction;
   
    //float direction;
    // Use this for initialization
   /* public override void Start()
    {
        wallMask = LayerMask.NameToLayer("Wall");
        Destroy(gameObject, 10);//auto destroy fire object after amount of time
        //Physics2D.IgnoreLayerCollision(gameObject.layer, 1<<gameObject.layer);
    }*/
    public override void SetDirection(float fraction, float direction)
    {
        base.SetDirection(fraction, direction);
        v_direction = new Vector3(1, fraction, 0);
        v_direction.Normalize();
    }
    // Update is called once per frame
    public override void FixedUpdate()
    {
        
        if (!mitFire.Hit)
        {
            /*if (Vector3.SqrMagnitude(this.transform.position - GameManager.Instance.MitPlayer.transform.position) > 10)
            {
                TrashMan.despawn(gameObject);
            }*/
            if (direction != 0)
            {
                // this.transform.position += v_direction * 4.5f * Time.deltaTime * direction;
                //rigid.AddForce(v_direction * 20.5f  * direction, ForceMode2D.Force);
                rigid.velocity = v_direction * 5.5f * direction;
            }
        }
    }
}
