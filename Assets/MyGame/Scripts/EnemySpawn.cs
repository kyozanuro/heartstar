﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour
{

    public GameObject[] enemyPrefabs;
    public float TimeInterval = 1;
    float timeCount;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timeCount += Time.deltaTime;
        if (timeCount >= TimeInterval)
        {
            //Vector2 rand = Random.insideUnitCircle * 2;
            Vector2 rand = new Vector2(2 * Mathf.Cos(Random.Range(0, Mathf.PI * 2)), 2 * Mathf.Sin(Random.Range(0, Mathf.PI * 2)));
            Vector3 position = new Vector3(rand.x, rand.y, transform.position.z) + transform.position;
            GameObject obj = GameObject.Instantiate(enemyPrefabs[Random.Range(0, enemyPrefabs.Length)], position, Quaternion.identity) as GameObject;
            obj.SendMessage("Init", transform.position, SendMessageOptions.DontRequireReceiver);
            obj.transform.parent = this.transform;
            timeCount -= TimeInterval;
        }

    }
}
