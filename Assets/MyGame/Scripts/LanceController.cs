﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using CreativeSpore.SmartColliders;
using System;

public class LanceController : MonoBehaviour
{
    public float CallLanceTime = 1;
    //[NonSerialized]
    public LanceState Calling = LanceState.NONE;

    public enum LanceState { NONE, CALLING, CALLED, THROWING, JUMPING, CANCELING }

    Transform staticLance;
    Transform staticLanceFigure;
    Transform dynamicLance;
    MitFire lanceMitFire;
    Animator animator;
    Sequence mySequence;
    DOTweenPath tweenPath;
    Vector3[] path;
    //SmartPlatformCollider smartPlatformCollider;
    PlatformCharacterController platformCharacterController;
    Transform lancePos;
    bool tweening;
    bool CallingFromNone;
    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        tweenPath = GetComponent<DOTweenPath>();
        dynamicLance = transform.Find("Mit_lance");
        dynamicLance.gameObject.SetActive(true);
        lanceMitFire = dynamicLance.GetComponent<MitFire>();
        staticLance = transform.Find("Mit_lance_static");
        staticLance.gameObject.SetActive(false);
        staticLanceFigure = transform.Find("Mit_lance_static_figure");
        staticLanceFigure.gameObject.SetActive(false);
        // smartPlatformCollider = GetComponent<SmartPlatformCollider>();

        DOTween.Init(true, true, LogBehaviour.ErrorsOnly);
        // smartPlatformCollider.enabled = false;
        mySequence = DOTween.Sequence();

        path = new Vector3[7];

        // path[4] = lancePos.position;
        /* tweenPath.hasOnComplete = true;
         tweenPath.onComplete = new UnityEngine.Events.UnityEvent();
         tweenPath.onComplete.AddListener(() =>
         {
             Calling = LanceState.CALLED;
             transform.parent = lancePos;
             transform.localPosition = Vector3.zero;
             transform.localRotation = Quaternion.identity;
             transform.localScale = new Vector3(1, 1, 1);
             SoundManager.Instance.condorSound1.Stop();
         });*/
        // mySequence.SetAutoKill()
    }

    public void Cancel()
    {
        transform.DOPause();
        Calling = LanceState.CANCELING;
        animator.ResetTrigger("Idle");
        animator.SetTrigger("None");

        // Hide();
        //if (SoundManager.Instance.condorSound1.time >= SoundManager.Instance.condorSound1.clip.length)
        {
            //SoundManager.Instance.condorSound1.Stop();
        }
    }

    public void Hide()
    {
        this.transform.parent = lancePos;
        this.transform.localPosition = new Vector3(0.5f, 2.7f, 0);
        Calling = LanceState.NONE;
        CallLanceTime = 1f;
    }


    public void Call(Transform lancePos)
    {
        if (this.lancePos == null)
        {
            this.lancePos = lancePos;
            platformCharacterController = this.lancePos.GetComponentInParent<PlatformCharacterController>();
        }
        if (Calling == LanceState.NONE || Calling == LanceState.THROWING || Calling == LanceState.JUMPING)
        {
            dynamicLance.gameObject.SetActive(true);
            staticLance.gameObject.SetActive(false);
            staticLanceFigure.gameObject.SetActive(false);
            transform.parent = null;
            animator.SetTrigger("Idle");
            //smartPlatformCollider.enabled = false;
            //mySequence.Kill();
            mySequence.Kill();
            if (Calling == LanceState.NONE)
            {
                CallingFromNone = true;
               // SoundManager.Instance.condorSound1.Play();
                path[0] = new Vector3(2, 1, 0) + lancePos.position;
                path[1] = new Vector3(1.8f, 0, 0) + lancePos.position;
                path[2] = new Vector3(1, -0.3f, 0) + lancePos.position;
                path[3] = new Vector3(0, -0.8f, 0) + lancePos.position;
                path[4] = new Vector3(-0.5f, -0.1f, 0) + lancePos.position;
                path[5] = new Vector3(-0.25f, 0.18f, 0) + lancePos.position;
                path[6] = lancePos.position;

                transform.DOKill();
                transform.DOPath(path, 1, PathType.CatmullRom, PathMode.Sidescroller2D).OnComplete(
                    () =>
                    {
                        Calling = LanceState.CALLED;
                        transform.parent = lancePos;
                        transform.localPosition = Vector3.zero;
                        transform.localRotation = Quaternion.identity;
                        transform.localScale = new Vector3(1, 1, 1);
                      //  SoundManager.Instance.condorSound1.Stop();
                    });

                transform.DOPlay();
                /*   mySequence = DOTween.Sequence();

                   mySequence.Append(transform.DOMove(lancePos.position, 1f)
              .SetEase(Ease.OutQuad).OnComplete(() =>
               {
                   Calling = LanceState.CALLED;
                   transform.parent = lancePos;
                   transform.localPosition = Vector3.zero;
                   transform.localRotation = Quaternion.identity;
                   transform.localScale = new Vector3(1, 1, 1);
                   SoundManager.Instance.condorSound1.Stop();
               }));

                   mySequence.Join(transform.DORotate(new Vector3(0, 0, -90), 0.03f).SetEase(Ease.Linear));*/
                //tweenPath.DOPlay();
            }
            else
            {
                CallingFromNone = false;
            }
            Calling = LanceState.CALLING;
            lanceMitFire.CanDamage = false;
            // mySequence = DOTween.Sequence();
            /* mySequence.Append(transform.DOMove(lancePos.position,0.5f*Vector2.Distance(transform.position, lancePos.position))).SetEase(Ease.InQuad).OnComplete(() =>
             {
                 calling = "called";
                 transform.parent = lancePos;
                 transform.localPosition = Vector3.zero;
                 transform.localRotation = Quaternion.identity;
                 transform.localScale = new Vector3(1, 1, 1);

             });*/
            //mySequence.Join(transform.DORotate(new Vector3(0, 0, 100), 0.01f, RotateMode.FastBeyond360).SetRelative().SetLoops(-1, LoopType.Incremental));
        }
    }

    public void Throw(float direction)
    {
        Calling = LanceState.THROWING;
        CallLanceTime = 0.5f;
        lanceMitFire.CanDamage = true;
        //smartPlatformCollider.enabled = true;
        animator.ResetTrigger("Idle");
        animator.SetTrigger("Throw");
        // mySequence.Restart();
        Debug.Log("Throw " + direction);
        transform.parent = null;
        transform.localScale = new Vector3(1, 1 * direction, 1);
       // SoundManager.Instance.condorSound2.Play();
        mySequence = DOTween.Sequence();

        //mySequence.Kill();
        //mySequence.Restart();
        // mySequence.Kill();
        mySequence.Append(transform.DOMove(new Vector3(1 * direction, 0, 0), 1f).
            SetRelative().SetEase(Ease.OutQuad).OnComplete(() =>
        {
            animator.SetTrigger("Idle");
        }));
        mySequence.Join(transform.DORotate(new Vector3(0, 0, -90), 0.03f).SetEase(Ease.Linear));

        mySequence.Append(transform.DOMove(new Vector3(-4 * direction, 0, 0), 2.5f).SetRelative().SetEase(Ease.InSine).OnComplete(() =>
        {
            Hide();
        }));
        mySequence.Join(transform.DOScaleY(-1 * direction, 0.01f).SetEase(Ease.Linear));
        mySequence.Play();


    }

    public void HitInvurnableEnemy()
    {
        //transform.DOPause();
        mySequence.Kill();
        mySequence = DOTween.Sequence();
        Calling = LanceState.CANCELING;
        mySequence.Append(transform.DOMove(new Vector3(-2, 3, 0), 2).SetRelative(true).OnComplete(() =>
        {
            Hide();
        }));
        mySequence.Join(transform.DORotate(new Vector3(0, 0, -3000), 2f, RotateMode.FastBeyond360).
            SetRelative(true).SetLoops(-1, LoopType.Restart));
        mySequence.Play();

    }

    void Update()
    {
        if (Calling == LanceState.CALLING)
        {
            if (!CallingFromNone)
            {
                transform.Rotate(new Vector3(0, 0, 1), 5000 * Time.deltaTime);
                Vector3 v = Vector3.zero;
                transform.position = Vector3.SmoothDamp(transform.position, lancePos.position, ref v, 0.06f);
                if (v.sqrMagnitude < 0.5f)
                {
                    Calling = LanceState.CALLED;
                    transform.parent = lancePos;
                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = new Vector3(1, 1, 1);
                  //  SoundManager.Instance.condorSound1.Stop();

                }
            }
            else
            {
                //Calling The Lance Frome None
                transform.Rotate(new Vector3(0, 0, 1), 5000 * Time.deltaTime);
            }

            //  Debug.Log(v);

        }

        // Debug.Log(SoundManager.Instance.condorSound1.time +", " + SoundManager.Instance.condorSound1.clip.length);
    }

    void OnCollisionEnter2D()
    {
        //Debug.Log("her!!!!!!!e");
        if (!tweening && (Calling == LanceState.JUMPING || Calling == LanceState.THROWING))
        {
            //  float dot = Vector3.Dot(transform.up, smartContactPoint.normal);
            //  Debug.Log("dot : " + dot);
            float vspeed = platformCharacterController.m_platformPhysics.VSpeed;
            // Debug.Log("hit lance" + vspeed);
            if (vspeed < 0)
            {
                tweening = true;
                // mySequence.Kill();
                mySequence.Kill();
                Calling = LanceState.JUMPING;
                dynamicLance.gameObject.SetActive(false);

                staticLance.gameObject.SetActive(true);
                staticLanceFigure.gameObject.SetActive(true);
                staticLanceFigure.localPosition = Vector3.zero;
                //smartContactPoint.otherCollider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                staticLanceFigure.DOMoveY(-0.1f, 0.06f).SetRelative().SetEase(Ease.InQuad).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
                 {
                     // Debug.Log("Completed");
                     tweening = false;
                     staticLanceFigure.localPosition = Vector3.zero;
                 });
                staticLanceFigure.DOPlay();
               // SoundManager.Instance.condorJump.Play();
            }
            else
            {
                Debug.Log("Wrong here");
            }
        }
    }
    /* void OnTriggerEnter2D(Collider2D other)
     {
         if (!tweening && (Calling == "none" || Calling == "throwing"))
         {
             Calling = "none";
             float vspeed = platformCharacterController.m_platformPhysics.VSpeed;
             if (vspeed < 0)
             {
                 mySequence.Kill();
                 dynamicLance.gameObject.SetActive(false);
                 staticLance.gameObject.SetActive(true);

             }
         }
     }*/

    void OnSmartTriggerStay2D(SmartContactPoint smartContactPoint)
    {
        if (!tweening && (Calling == LanceState.JUMPING || Calling == LanceState.THROWING))
        {

            float vspeed = platformCharacterController.m_platformPhysics.VSpeed;
            // Debug.Log("hit lance" + vspeed);
            if (vspeed < 0)
            {

                tweening = true;
                //mySequence.Kill();
                mySequence.Kill();

                Calling = LanceState.JUMPING;
                dynamicLance.gameObject.SetActive(false);

                staticLance.gameObject.SetActive(true);
                staticLanceFigure.gameObject.SetActive(true);
                staticLanceFigure.localPosition = Vector3.zero;
                //smartContactPoint.otherCollider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 10, ForceMode2D.Impulse);
                staticLanceFigure.DOMoveY(-0.1f, 0.06f).SetRelative().SetEase(Ease.InQuad).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
                {
                    // Debug.Log("Completed");
                    tweening = false;
                    staticLanceFigure.localPosition = Vector3.zero;
                });
                staticLanceFigure.DOPlay();
                //SoundManager.Instance.condorJump.Play();
            }
        }
    }

}
