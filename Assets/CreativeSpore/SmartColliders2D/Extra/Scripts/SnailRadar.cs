﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailRadar : MonoBehaviour
{
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //radar reach player
            gameObject.SendMessageUpwards("OnRadarTriggerEnter");
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            //radar reach player
            gameObject.SendMessageUpwards("OnRadarTriggerExit");
        }
    }
}
