﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

namespace CreativeSpore.SmartColliders
{
    [RequireComponent(typeof(PlatformCharacterController))]
    public class PlatformCharacterInput : MonoBehaviour
    {
        public enum eInputMode
        {
            Keyboard,
            Gamepad
        }
        public eInputMode InputMode = eInputMode.Gamepad;

        /// <summary>
        /// If true, the moving speed will be proportional to the axis value
        /// </summary>
        public bool UseAxisAsSpeedFactor = true;
        /// <summary>
        /// Minimum axis value to start moving
        /// </summary>
        public float AxisMovingThreshold = 0.2f;

        private PlatformCharacterController m_platformCtrl;
        private CharacterController character;
        void Start()
        {
            m_platformCtrl = GetComponent<PlatformCharacterController>();
            character = GetComponent<CharacterController>();
        }

        public void Disable()
        {

            this.enabled = false;
            m_platformCtrl.SetActionState(eControllerActions.Left, false);
            m_platformCtrl.SetActionState(eControllerActions.Right, false);
            m_platformCtrl.SetActionState(eControllerActions.Up, false);
            m_platformCtrl.SetActionState(eControllerActions.Down, false);
            m_platformCtrl.SetActionState(eControllerActions.Jump, false);

        }
        public void Enable()
        {
            this.enabled = true;
        }
        void FixedUpdate()
        {
            //+++Autodetecting input device. Comment or remove this to manually specify the input management
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
            {
                InputMode = eInputMode.Keyboard;
            }
            else if (Input.GetKey(KeyCode.Joystick1Button0))
            {
                InputMode = eInputMode.Gamepad;
            }
            //---
            if (InputMode == eInputMode.Gamepad)
            {
                float fHorAxis = Input.GetAxis("Horizontal"); fHorAxis *= Mathf.Abs(fHorAxis);
                float fVerAxis = Input.GetAxis("Vertical"); fVerAxis *= Mathf.Abs(fVerAxis);
                float fAbsHorAxis = Mathf.Abs(fHorAxis);
                float fAbsVerAxis = Mathf.Abs(fVerAxis);

                if (fAbsHorAxis >= AxisMovingThreshold)
                    m_platformCtrl.HorizontalSpeedScale = UseAxisAsSpeedFactor ? fAbsHorAxis : 1f;
                if (fAbsVerAxis >= AxisMovingThreshold)
                    m_platformCtrl.VerticalSpeedScale = UseAxisAsSpeedFactor ? fAbsVerAxis : 1f;

                m_platformCtrl.SetActionState(eControllerActions.Left, fHorAxis <= -AxisMovingThreshold);
                m_platformCtrl.SetActionState(eControllerActions.Right, fHorAxis >= AxisMovingThreshold);
                m_platformCtrl.SetActionState(eControllerActions.Down, fVerAxis <= -AxisMovingThreshold);
                m_platformCtrl.SetActionState(eControllerActions.Up, fVerAxis >= AxisMovingThreshold);

                m_platformCtrl.SetActionState(eControllerActions.PlatformDropDown, (Input.GetButton("Fire1") || Input.GetButton("Jump")) && (fVerAxis <= -AxisMovingThreshold));
                m_platformCtrl.SetActionState(eControllerActions.Jump, (Input.GetButton("Fire1") || Input.GetButton("Jump")) && !(fVerAxis <= -AxisMovingThreshold));
            }
            else //if( InputMode == eInputMode.Keyboard )
            {
                if (character.IsActive)
                {
                    character.Attacking = CrossPlatformInputManager.GetButton("Fire1");

                    m_platformCtrl.HorizontalSpeedScale = m_platformCtrl.VerticalSpeedScale = 1f;
                    // if (!character.Attacking && !character.Hurt)
                    {
                        // Debug.Log("CrossPlatformInputManager.GetAxisRaw(Horizontal) " + CrossPlatformInputManager.GetAxisRaw("Horizontal"));
                        if (m_platformCtrl.IsGrounded)
                        {
                            m_platformCtrl.SetActionState(eControllerActions.Left, CrossPlatformInputManager.GetAxisRaw("Horizontal") < 0);
                            m_platformCtrl.SetActionState(eControllerActions.Right, CrossPlatformInputManager.GetAxisRaw("Horizontal") > 0);
                        }
                        else
                        {
                            m_platformCtrl.SetActionState(eControllerActions.Left, CrossPlatformInputManager.GetAxis("Horizontal") < 0);
                            m_platformCtrl.SetActionState(eControllerActions.Right, CrossPlatformInputManager.GetAxis("Horizontal") > 0);
                        }
                        //  m_platformCtrl.SetActionState(eControllerActions.Left, Input.GetKey(KeyCode.LeftArrow));
                        //  m_platformCtrl.SetActionState(eControllerActions.Right, Input.GetKey(KeyCode.RightArrow));

                        // m_platformCtrl.SetActionState(eControllerActions.Right, CrossPlatformInputManager.GetAxis("Horizontal") > 0);
                        m_platformCtrl.SetActionState(eControllerActions.Up, Input.GetKey(KeyCode.UpArrow));
                        m_platformCtrl.SetActionState(eControllerActions.Down, Input.GetKey(KeyCode.DownArrow));
                    }
                    /*else
                    {
                        m_platformCtrl.SetActionState(eControllerActions.Left, false);
                        m_platformCtrl.SetActionState(eControllerActions.Right, false);
                    }*/


                    //Don't allow jumping when CallingLance
                    // if (!character.CallingLance)
                    {
                        m_platformCtrl.SetActionState(eControllerActions.PlatformDropDown, CrossPlatformInputManager.GetButton("Jump") && Input.GetKey(KeyCode.DownArrow));
                        m_platformCtrl.SetActionState(eControllerActions.Jump, CrossPlatformInputManager.GetButton("Jump") && !Input.GetKey(KeyCode.DownArrow));
                    }
                    // else
                    // {
                    // m_platformCtrl.SetActionState(eControllerActions.Jump, false);
                    // }
                }
                else
                {
                    m_platformCtrl.SetActionState(eControllerActions.Jump, false);
                    m_platformCtrl.SetActionState(eControllerActions.Left, false);
                    m_platformCtrl.SetActionState(eControllerActions.Right, false);
                    m_platformCtrl.SetActionState(eControllerActions.Up, false);
                    m_platformCtrl.SetActionState(eControllerActions.Down, false);
                }


            }
        }
    }
}